package org.c0d3r.crates.listeners;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.c0d3r.crates.Main;
import org.c0d3r.crates.utils.LocationUtils;
import org.c0d3r.crates.utils.MessageUtils;

public class BreakListener implements Listener {

    @EventHandler
    public void onBlockBreak(BlockBreakEvent e) {
        Player player = e.getPlayer();
        Location location = e.getBlock().getLocation();
        String locationString = LocationUtils.locationToString(location);
        if (Main.getInstance().getCrateLocations().getString("locations." + locationString + ".crate") != null) {
            if (!player.isOp() || !player.hasPermission("c0d3rcrates.admin")) {
                e.setCancelled(true);
                return;
            }
            if (!player.isSneaking()) {
                e.setCancelled(true);
                MessageUtils.getInstance().sendMessageList(player, "SNEAK_BREAK");
            } else {
                if (Main.getInstance().getCrateLocations().getString("locations." + locationString + ".crate") != null) {
                    Main.getInstance().getCrateLocations().set("locations." + locationString, null);
                    Main.getInstance().saveCrateLocations();
                    Main.getInstance().reloadCrateLocations();
                    Main.getInstance().getCrates().deleteHolograms();
                    Main.getInstance().getCrates().deleteParticles();
                    Main.getInstance().getCrates().reInitLocations();
                    MessageUtils.getInstance().sendMessageList(player, "CRATE_BROKE");
                } else player.sendMessage("§4A weird error occurred failed to delete crate block (if it is one)...");
            }
        }
    }

}
