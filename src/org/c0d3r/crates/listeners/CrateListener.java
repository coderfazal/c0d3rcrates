package org.c0d3r.crates.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.c0d3r.crates.api.events.CrateOpenEvent;
import org.c0d3r.crates.api.events.CratePreOpenEvent;

public class CrateListener implements Listener {

    @EventHandler
    public void onCratePreOpen(CratePreOpenEvent event) {
        if (event.isCancelled()) {
            return;
        }
        event.getAnimation().setRewards(event.getCrate().getItemList());
        event.getAnimation().start();
        CrateOpenEvent.fireEvent(event.getPlayer(), event.getCrate().getCrateKey());
    }

}
