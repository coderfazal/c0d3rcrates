package org.c0d3r.crates.listeners;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.c0d3r.crates.Main;

public class JoinListener implements Listener {

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        if (!Main.getInstance().getConfig().getBoolean("updateCheck")) return;
        Player player = e.getPlayer();
        if (player.isOp() || player.hasPermission("c0d3rcrates.admin")) {
            Bukkit.getScheduler().scheduleSyncDelayedTask(Main.getInstance(), () -> {
                if (Main.getInstance().getUpdate().isUpdated()) {
                    player.sendMessage("§2§l[C0d3rCrates] §aYou are using the latest version of C0d3rCrates!");
                } else {
                    player.sendMessage("§2§l[C0d3rCrates] §aYou are using an outdated version of C0d3rCrates!");
                    player.sendMessage("§2§l[C0d3rCrates] §aCurrent Version:§f " + Main.getInstance().getDescription().getVersion());
                    player.sendMessage("§2§l[C0d3rCrates] §aNew Version:§f " + Main.getInstance().getUpdate().getVersionFromURL());
                }
            }, 100L);
        }
    }

}
