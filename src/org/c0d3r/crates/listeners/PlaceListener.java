package org.c0d3r.crates.listeners;

import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.inventory.ItemStack;
import org.c0d3r.crates.Main;
import org.c0d3r.crates.crate.Crate;
import org.c0d3r.crates.utils.LocationUtils;
import org.c0d3r.crates.utils.MessageUtils;

public class PlaceListener implements Listener {

    @EventHandler
    public void onPlace(BlockPlaceEvent e) {
        ItemStack item = e.getItemInHand();
        if (e.getPlayer().isOp() || e.getPlayer().hasPermission("c0d3rcrates.admin")) {
            Crate crate = Main.getInstance().getCrates().crateBlockToCrate(item);
            if (crate != null) {
                Location location = e.getBlock().getLocation();
                String locationString = LocationUtils.locationToString(location);
                Main.getInstance().getCrateLocations().set("locations." + locationString + ".crate", Main.getInstance().getCrates().crateToString(crate));
                Main.getInstance().saveCrateLocations();
                Main.getInstance().reloadCrateLocations();
                Main.getInstance().getCrates().deleteHolograms();
                Main.getInstance().getCrates().deleteParticles();
                Main.getInstance().getCrates().reInitLocations();
                MessageUtils.getInstance().sendMessageList(e.getPlayer(), "PLACED_CRATE", "%location%", locationString);
            }
        }
    }

}
