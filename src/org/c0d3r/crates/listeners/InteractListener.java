package org.c0d3r.crates.listeners;

import com.gmail.filoghost.holographicdisplays.api.HologramsAPI;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.c0d3r.crates.Main;
import org.c0d3r.crates.animation.Animation;
import org.c0d3r.crates.animation.Animations;
import org.c0d3r.crates.animation.handler.AnimationHandler;
import org.c0d3r.crates.animation.player.AnimationManager;
import org.c0d3r.crates.animation.player.AnimationPlayer;
import org.c0d3r.crates.animation.pretask.CSGOSelectSlotPick;
import org.c0d3r.crates.animation.pretask.ReverseCSGOSelectSlotPick;
import org.c0d3r.crates.animation.pretask.ThreeByThreeSlotPick;
import org.c0d3r.crates.animation.settings.AnimationSettings;
import org.c0d3r.crates.animation.types.*;
import org.c0d3r.crates.api.events.CratePreOpenEvent;
import org.c0d3r.crates.crate.Crate;
import org.c0d3r.crates.crate.CrateReward;
import org.c0d3r.crates.data.DataManager;
import org.c0d3r.crates.data.DataPlayer;
import org.c0d3r.crates.menu.CrateMenu;
import org.c0d3r.crates.menus.InventoryClickType;
import org.c0d3r.crates.menus.Menu;
import org.c0d3r.crates.menus.MenuAPI;
import org.c0d3r.crates.menus.MenuItem;
import org.c0d3r.crates.utils.ItemBuilder;
import org.c0d3r.crates.utils.LocationUtils;
import org.c0d3r.crates.utils.MessageUtils;
import org.c0d3r.crates.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class InteractListener implements Listener {

    @EventHandler
    public void onInteractPhysKey(PlayerInteractEvent e) {
        if (Main.getInstance().getConfig().getString("mode").equalsIgnoreCase("physical")) return;
        check(e.getPlayer(), e);
    }

    @EventHandler
    public void onInteractOpenCrate(PlayerInteractEvent e) {
        if (!e.hasBlock() || e.getClickedBlock() == null) return;
        Location location = e.getClickedBlock().getLocation();
        if (location == null) return;
        String locationString = LocationUtils.locationToString(location);
        if (Main.getInstance().getCrateLocations().getString("locations." + locationString + ".crate") != null) {
            e.setCancelled(true);
            if (e.getAction() == Action.RIGHT_CLICK_BLOCK) {
                if (Utils.canUse(e.getPlayer())) {
                    if (Main.getInstance().getConfig().getString("mode").equalsIgnoreCase("virtual")) {
                        DataPlayer dataPlayer = DataManager.getInstance().getByPlayer(e.getPlayer());
                        if (dataPlayer != null) {
                            new CrateMenu(e.getPlayer(), dataPlayer);
                        }
                    } else {
                        Crate crate = Main.getInstance().getCrates().physKeyToCrate(e.getPlayer().getItemInHand());
                        if (crate != null) {
                            if (Main.getInstance().getCrateLocations().getString("locations." + locationString + ".crate").equals(Main.getInstance().getCrates().crateToString(crate))) {
                                check(e.getPlayer(), e);
                            } else {
                                MessageUtils.getInstance().sendMessageList(e.getPlayer(), "WRONG_KEY");
                            }
                        } else {
                            MessageUtils.getInstance().sendMessageList(e.getPlayer(), "NEED_KEY_TO_OPEN");
                        }
                    }
                }
            } else if (e.getAction() == Action.LEFT_CLICK_BLOCK && !e.getPlayer().isSneaking() && Main.getInstance().getConfig().getBoolean("preview-left-click-crate-block")) {
                Crate crate = Main.getInstance().getCrates().stringToCrate(Main.getInstance().getCrateLocations().getString("locations." + locationString + ".crate"));
                if (crate != null) {
                    preview(e.getPlayer(), crate);
                }
            }
        }
    }

    private void preview(Player player, Crate crate) {
        Menu previewMenu = MenuAPI.getInstance().createMenu(Utils.toColor(crate.getPreviewTitle()), Utils.inventoryRowCalculator(crate.getItemList().size()));
        if (crate.getItemList().getMap().values().size() > 45) {
            List<MenuItem> items = new ArrayList<>();
            for (CrateReward reward : crate.getItemList().getMap().values()) {
                ItemStack itemStack = reward.getItem();
                items.add(new MenuItem() {
                    @Override
                    public void onClick(Player player, InventoryClickType clickType) {
                        player.updateInventory();
                        player.closeInventory();
                    }

                    @Override
                    public ItemStack getItemStack() {
                        return itemStack;
                    }
                });
            }
            previewMenu.addMenuItem(new MenuItem() {
                @Override
                public void onClick(Player player1, InventoryClickType clickType) {
                    previewMenu.previousPage(player1);
                }

                @Override
                public ItemStack getItemStack() {
                    return new ItemBuilder(Material.getMaterial(Main.getInstance().getConfig().getString("items.preview-menu.previous.material")), Main.getInstance().getConfig().getInt("items.preview-menu.previous.data")).setName(Main.getInstance().getConfig().getString("items.preview-menu.previous.name")).setLore(Main.getInstance().getConfig().getStringList("items.preview-menu.previous.lore")).getStack();
                }
            }, previewMenu.getInventory().getSize() - 6);
            previewMenu.addMenuItem(new MenuItem() {
                @Override
                public void onClick(Player player1, InventoryClickType clickType) {
                    previewMenu.nextPage(player1);
                }

                @Override
                public ItemStack getItemStack() {
                    return new ItemBuilder(Material.getMaterial(Main.getInstance().getConfig().getString("items.preview-menu.next.material")), Main.getInstance().getConfig().getInt("items.preview-menu.next.data")).setName(Main.getInstance().getConfig().getString("items.preview-menu.next.name")).setLore(Main.getInstance().getConfig().getStringList("items.preview-menu.next.lore")).getStack();
                }
            }, previewMenu.getInventory().getSize() - 4);
            List<Integer> scrollSlots = new ArrayList<>();
            for (int i = 0; i < previewMenu.getInventory().getSize() - 9; i++) {
                scrollSlots.add(i);
            }
            previewMenu.setupPages(items, scrollSlots);
        } else {
            int slot = 0;
            for (CrateReward reward : crate.getItemList().getMap().values()) {
                ItemStack itemStack = reward.getItem();
                MenuItem icon = new MenuItem() {
                    @Override
                    public void onClick(Player player, InventoryClickType clickType) {
                        player.updateInventory();
                        player.closeInventory();
                    }

                    @Override
                    public ItemStack getItemStack() {
                        return itemStack;
                    }
                };
                previewMenu.addMenuItem(icon, slot);
                slot++;
            }
        }
        previewMenu.addMenuItem(new MenuItem() {
            @Override
            public void onClick(Player p0, InventoryClickType p1) {
                p0.updateInventory();
                p0.closeInventory();
            }

            @Override
            public ItemStack getItemStack() {
                return new ItemBuilder(Material.getMaterial(Main.getInstance().getConfig().getString("items.preview-menu.mainmenu.material")), Main.getInstance().getConfig().getInt("items.preview-menu.mainmenu.data")).setName(Main.getInstance().getConfig().getString("items.preview-menu.mainmenu.name")).setLore(Main.getInstance().getConfig().getStringList("items.preview-menu.mainmenu.lore")).getStack();
            }
        }, previewMenu.getInventory().getSize() - 5);
        player.closeInventory();
        previewMenu.openMenu(player);
    }

    private void check(Player player, PlayerInteractEvent e) {
        if (player.getItemInHand() != null) {
            Crate crate = Main.getInstance().getCrates().physKeyToCrate(player.getItemInHand());
            if (crate != null) {
                e.setCancelled(true);
                if (e.getAction() == Action.LEFT_CLICK_AIR || e.getAction() == Action.LEFT_CLICK_BLOCK) {
                    preview(player, crate);
                }
                if (!Utils.canUse(player)) {
                    return;
                }
                if (player.getItemInHand().getAmount() == 1) {
                    player.setItemInHand(new ItemStack(Material.AIR, 1));
                } else {
                    ItemStack itemStack = player.getItemInHand();
                    itemStack.setAmount(itemStack.getAmount() - 1);
                    player.setItemInHand(itemStack);
                }
                AnimationPlayer animationPlayer = AnimationManager.createAnimationPlayer(player, crate);
                animationPlayer.add();
                if (crate.getPresetAnimation()) {
                    Animations animations = crate.getAnimation();
                    animationPlayer.setAnimations(animations);
                    Animation animation = null;
                    Inventory crateInventory;
                    if (animations == Animations.CSGO) {
                        crateInventory = Bukkit.createInventory(null, animations.getInvSize(), Utils.toColor(crate.getTitle()));
                        AnimationSettings animationSettings = new AnimationSettings();
                        animation = new CSGOAnimation(player, crate, crateInventory, animationSettings);
                    } else if (animations == Animations.INSTANT) {
                        animation = new InstantAnimation(player, crate);
                    } else if (animations == Animations.REVERSECSGO) {
                        crateInventory = Bukkit.createInventory(null, animations.getInvSize(), Utils.toColor(crate.getTitle()));
                        AnimationSettings animationSettings = new AnimationSettings();
                        animation = new ReverseCSGOAnimation(player, crate, crateInventory, animationSettings);
                    } else if (animations == Animations.ROULETTE) {
                        crateInventory = Bukkit.createInventory(null, animations.getInvSize(), Utils.toColor(crate.getTitle()));
                        AnimationSettings animationSettings = new AnimationSettings();
                        animation = new RouletteAnimation(player, crate, crateInventory, animationSettings);
                    } else if (animations == Animations.WHEELOFFORTUNE) {
                        crateInventory = Bukkit.createInventory(null, animations.getInvSize(), Utils.toColor(crate.getTitle()));
                        AnimationSettings animationSettings = new AnimationSettings();
                        animation = new WOFAnimation(player, crate, crateInventory, animationSettings);
                    } else if (animations == Animations.SHRINK) {
                        crateInventory = Bukkit.createInventory(null, animations.getInvSize(), Utils.toColor(crate.getTitle()));
                        animation = new ShrinkAnimation(player, crate, crateInventory);
                    } else if (animations == Animations.DOUBLECSGO) {
                        AnimationSettings animationSettings = new AnimationSettings();
                        crateInventory = Bukkit.createInventory(null, animations.getInvSize(), Utils.toColor(crate.getTitle()));
                        animation = new DoubleCSGOAnimation(player, crate, crateInventory, animationSettings);
                    } else if (animations == Animations.THREEBYTHREE) {
                        player.closeInventory();
                        animationPlayer.setSlotTitle(Utils.toColor(Main.getInstance().getConfig().getString("menus.slotPicker.title").replace("%slots%", crate.getMaxRewards3BY3() + "")));
                        ThreeByThreeSlotPick.getInstance().openSlotPicker(player);
                        return;
                    } else if (animations == Animations.SLOT) {
                        crateInventory = Bukkit.createInventory(null, animations.getInvSize(), Utils.toColor(crate.getTitle()));
                        AnimationSettings animationSettings = new AnimationSettings();
                        animation = new SlotAnimation(player, crate, crateInventory, animationSettings);
                    } else if (animations == Animations.CSGOSELECT) {
                        player.closeInventory();
                        animationPlayer.setSlotTitle(Utils.toColor(Main.getInstance().getConfig().getString("menus.slotPicker.title").replace("%slots%", crate.getMaxRewardsCSGOSelect() + "")));
                        CSGOSelectSlotPick.getInstance().openSlotPicker(player);
                        return;
                    } else if (animations == Animations.REVERSECSGOSELECT) {
                        player.closeInventory();
                        animationPlayer.setSlotTitle(Utils.toColor(Main.getInstance().getConfig().getString("menus.slotPicker.title").replace("%slots%", crate.getMaxRewardsReverseCSGOSelect() + "")));
                        ReverseCSGOSelectSlotPick.getInstance().openSlotPicker(player);
                        return;
                    } else if (animations == Animations.PHYSICAL) {
                        AnimationSettings animationSettings = new AnimationSettings();
                        animation = new PhsyicalAnimation(player, crate, HologramsAPI.createHologram(Main.getInstance(), player.getLocation().clone().add(0.5, 3.33, 0.5)), animationSettings);
                    }
                    if (animation != null) {
                        CratePreOpenEvent.fireEvent(player, crate, animation);
                    }
                } else {
                    AnimationHandler.getInstance().openAnimationPicker(player);
                }
            }
        }
    }

}