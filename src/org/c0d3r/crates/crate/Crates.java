package org.c0d3r.crates.crate;

import com.gmail.filoghost.holographicdisplays.api.Hologram;
import com.gmail.filoghost.holographicdisplays.api.HologramsAPI;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitTask;
import org.c0d3r.crates.Main;
import org.c0d3r.crates.utils.LocationUtils;
import org.c0d3r.crates.utils.Utils;
import org.c0d3r.crates.utils.hologram.HologramsEffect;
import org.c0d3r.crates.utils.particle.ParticleEffect;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Crates {

    private final List<BukkitTask> particleTasks = new ArrayList<>();
    private final List<Integer> hologramTasks = new ArrayList<>();
    private List<Crate> crateList = new ArrayList<>();
    private Map<ItemStack, Crate> crateItemCrateMap = new HashMap<>();
    private List<String> crateLocations = new ArrayList<>();
    private List<String> crateNameList = new ArrayList<>();
    private Map<String, Hologram> holograms = new HashMap<>();

    public Crates() {
        for (String crateKey : Main.getInstance().getConfig().getConfigurationSection("crates").getKeys(false)) {
            Crate crate = new Crate(crateKey);
            crateList.add(crate);
            crateNameList.add(crateKey);
            crateItemCrateMap.put(crate.getCrateItem(), crate);
        }
    }

    public void initLocations() {
        if (Main.getInstance().getCrateLocations().getConfigurationSection("locations") == null) return;
        for (String locationKey : Main.getInstance().getCrateLocations().getConfigurationSection("locations").getKeys(false)) {
            String crateName = Main.getInstance().getCrateLocations().getString("locations." + locationKey + ".crate");
            Location location = LocationUtils.stringtoLocation(locationKey);
            this.crateLocations.add(locationKey);
            //this.initParticles(location, crateName);
            boolean rewardsEnabled = Main.getInstance().getConfig().getBoolean("crates." + crateName + ".settings.hologram.enabled");
            List<String> lines = Main.getInstance().getConfig().getStringList("crates." + crateName + ".settings.hologram.info");
            double spacing = 0.666666667 * (rewardsEnabled ? (lines.size() + 1.5) : lines.size());
            this.holograms.put(crateName, HologramsAPI.createHologram(Main.getInstance(), location.clone().add(0.5, spacing, 0.5)));
            lines.stream().map(Utils::toColor).forEach(line -> holograms.get(crateName).appendTextLine(line));
            if (rewardsEnabled) {
                HologramsEffect effect = new HologramsEffect(crateName, location, Main.getInstance().getConfig().getString("crates." + crateName + ".settings.hologram.format", "&r%reward%"));
                addEffect(effect);
            }
            System.out.print("Registered " + locationKey + " for Crate:" + crateName);
        }
    }

    private void addEffect(HologramsEffect effect) {
        this.hologramTasks.add(effect.runTaskTimerAsynchronously(Main.getInstance(), 15L, 15L).getTaskId());
    }

    public void reInitLocations() {
        this.crateLocations.clear();
        this.initLocations();
    }

    public Crate physKeyToCrate(ItemStack item) {
        if (!item.hasItemMeta()) return null;
        for (Crate crate : crateList) {
            ItemStack itemStack = crate.getPhysicalKey();
            if (item.getItemMeta().equals(itemStack.getItemMeta())) {
                return crate;
            }
        }
        return null;
    }

    public void deleteHolograms() {
        for (Hologram hologram : this.holograms.values()) {
            hologram.delete();
        }
        for (Hologram hologram : HologramsAPI.getHolograms(Main.getInstance())) {
            hologram.delete();
        }
        for (int id : this.hologramTasks) {
            Bukkit.getScheduler().cancelTask(id);
        }
        this.hologramTasks.clear();
        this.holograms.clear();
    }

    public void deleteParticles() {
        for (BukkitTask task : this.particleTasks) {
            task.cancel();
        }
        this.particleTasks.clear();
    }

    public void reInitHolograms() {
        deleteHolograms();
        for (String locationKey : Main.getInstance().getCrateLocations().getConfigurationSection("locations").getKeys(false)) {
            String crateName = Main.getInstance().getCrateLocations().getString("locations." + locationKey + ".crate");
            Location location = LocationUtils.stringtoLocation(locationKey);
            HologramsEffect effect = new HologramsEffect(crateName, location, Main.getInstance().getConfig().getString("crates." + crateName + ".settings.hologram.format", "&r%reward%"));
            addEffect(effect);
        }
    }

    private void initParticles(Location location, String crateName) {
        Crate crate = this.stringToCrate(crateName);
        if (crate != null) {
            BukkitTask bukkitTask = Bukkit.getScheduler().runTaskTimerAsynchronously(Main.getInstance(), () -> playParticles(location, crate.getParticleName()), 0L, 20L);
            this.particleTasks.add(bukkitTask);
        }
    }

    private void playParticles(Location location, String particleName) {
        for (Player player : location.getWorld().getPlayers()) {
            if (player.getLocation().distance(location) <= 16) {
                ParticleEffect.valueOf(particleName).display(0.25f, 0.25f, 0.25f, 1.0f, 15, location, player);
            }
        }
    }

    public void reInitParticles() {
        for (BukkitTask task : this.particleTasks) {
            task.cancel();
        }
        for (String locationKey : Main.getInstance().getCrateLocations().getConfigurationSection("locations").getKeys(false)) {
            Location location = LocationUtils.stringtoLocation(locationKey);
            this.initParticles(location, Main.getInstance().getCrateLocations().getString("locations." + locationKey + ".crate"));
        }
    }

    public List<String> getCrateNameList() {
        return crateNameList;
    }

    public void setCrateNameList(List<String> crateNameList) {
        this.crateNameList = crateNameList;
    }

    public List<Crate> getCrateList() {
        return crateList;
    }

    public void setCrateList(List<Crate> crateList) {
        this.crateList = crateList;
    }

    public List<String> getCrateLocations() {
        return crateLocations;
    }

    public void setCrateLocations(List<String> crateLocations) {
        this.crateLocations = crateLocations;
    }

    public Crate stringToCrate(String name) {
        for (Crate crate : this.crateList) {
            if (crate.getCrateKey().equalsIgnoreCase(name)) {
                return crate;
            }
        }
        return null;
    }

    public String crateToString(Crate crate) {
        if (crate != null) return crate.getCrateKey();
        return null;
    }

    public Crate crateBlockToCrate(ItemStack item) {
        ItemStack itemClone = item.clone();
        itemClone.setAmount(1);
        if (this.crateItemCrateMap.containsKey(itemClone)) {
            return this.crateItemCrateMap.get(itemClone);
        }
        return null;
    }


    public Map<ItemStack, Crate> getCrateItemCrateMap() {
        return crateItemCrateMap;
    }

    public void setCrateItemCrateMap(Map<ItemStack, Crate> crateItemCrateMap) {
        this.crateItemCrateMap = crateItemCrateMap;
    }

    public void reload() {
        crateList.clear();
        crateItemCrateMap.clear();
        crateNameList.clear();
        for (String crateKey : Main.getInstance().getConfig().getConfigurationSection("crates").getKeys(false)) {
            Crate crate = new Crate(crateKey);
            crateList.add(crate);
            crateNameList.add(crateKey);
            crateItemCrateMap.put(crate.getCrateItem(), crate);
        }
        reInitLocations();
    }

    public void destroy() {
        this.crateList.clear();
        this.crateList = null;
        crateItemCrateMap.clear();
        crateItemCrateMap = null;
        crateNameList.clear();
        crateNameList = null;
    }

}
