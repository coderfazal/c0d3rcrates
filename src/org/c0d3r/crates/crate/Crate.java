package org.c0d3r.crates.crate;

import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.inventory.ItemStack;
import org.c0d3r.crates.Main;
import org.c0d3r.crates.animation.Animations;
import org.c0d3r.crates.utils.EnchantGlow;
import org.c0d3r.crates.utils.ItemBuilder;
import org.c0d3r.crates.utils.ItemUtils;
import org.c0d3r.crates.utils.RandomPick;

import java.util.*;

public class Crate {

    private int slot;
    private String crateKey;

    private ItemStack physicalKey;
    private ItemStack menuItem;
    private ItemStack borderItem;
    private ItemStack prizeItem;
    private ItemStack crateItem;

    private List<Integer> borderDataList;
    private List<CrateReward> rewardList;

    private String title;
    private String previewTitle;
    private String particleName;
    private String hologramFormat;

    private Boolean presetAnimation;
    private Boolean hologramEnabled;
    private Boolean amountPlayer;

    private Animations animation;

    private int maxRewards3BY3;
    private int maxRewardsCSGOSelect;
    private int maxRewardsReverseCSGOSelect;
    private int maxRewardsInstant;

    private double worth;

    private List<Integer> dataList;

    private Iterator<Integer> dataIterator;

    private RandomPick<CrateReward> itemList = new RandomPick<>();

    private Map<CrateReward, Double> chanceMap;

    public Crate(String crateKey) {
        ConfigurationSection section = Main.getInstance().getConfig().getConfigurationSection("crates." + crateKey);
        String title = section.getString("title");
        String menuMaterial = section.getString("displayItem.material");
        int menuData = 0;
        if (section.get("displayItem.data") instanceof List) {
            dataList = new ArrayList<>(section.getIntegerList("displayItem.data"));
            dataIterator = dataList.iterator();
        } else {
            menuData = section.getInt("displayItem.data");
            if (menuData == 0) {
                menuData = ItemUtils.getInstance().getData(menuMaterial);
            }
        }
        int menuAmount = section.getInt("displayItem.amount");
        this.amountPlayer = menuAmount == -1;
        int slot = section.getInt("slot");
        String menuName = section.getString("displayItem.name");
        List<String> menuLore = section.getStringList("displayItem.lore");
        String physMaterial = section.getString("globalKey.material");
        int physData = section.getInt("globalKey.data");
        String physName = section.getString("globalKey.name");
        List<String> physLore = section.getStringList("globalKey.lore");
        this.title = title;
        this.particleName = section.getString("settings.particle");
        this.hologramEnabled = section.getBoolean("settings.hologram.enabled");
        this.hologramFormat = section.getString("settings.hologram.format");
        this.presetAnimation = section.getBoolean("settings.animations.presetAnimation");
        this.maxRewards3BY3 = section.getInt("settings.animations.settings.3BY3.rewards");
        this.maxRewardsCSGOSelect = section.getInt("settings.animations.settings.CSGO-SELECT.rewards");
        this.maxRewardsReverseCSGOSelect = section.getInt("settings.animations.settings.REVERSE-CSGO-SELECT.rewards");
        this.maxRewardsInstant = section.getInt("settings.animations.settings.INSTANT.rewards");
        this.worth = section.getDouble("settings.gamble.worth", 1.50);
        if (maxRewards3BY3 > 9) {
            maxRewards3BY3 = 9;
        } else if (maxRewards3BY3 < 1) {
            maxRewards3BY3 = 1;
        }
        if (maxRewardsCSGOSelect > 9) {
            maxRewardsCSGOSelect = 9;
        } else if (maxRewardsCSGOSelect < 1) {
            maxRewardsCSGOSelect = 1;
        }
        if (maxRewardsReverseCSGOSelect > 5) {
            maxRewardsReverseCSGOSelect = 5;
        } else if (maxRewardsReverseCSGOSelect < 1) {
            maxRewardsReverseCSGOSelect = 1;
        }
        if (this.presetAnimation) {
            this.animation = Animations.getByName(section.getString("settings.animations.presetAnimationName"));
        }
        this.crateKey = crateKey;
        this.slot = slot;
        this.menuItem = new ItemBuilder(ItemUtils.getInstance().getMaterial(menuMaterial), menuData).setName(menuName).setLore(menuLore).setAmount(menuAmount).getStack();
        this.physicalKey = new ItemBuilder(Material.getMaterial(physMaterial), physData).setName(physName).setLore(physLore).getStack();
        if (section.getBoolean("displayItem.enchanted")) {
            EnchantGlow.addGlow(this.menuItem);
        }
        if (section.getBoolean("globalKey.enchanted")) {
            EnchantGlow.addGlow(this.physicalKey);
        }
        this.rewardList = new ArrayList<>();
        this.chanceMap = new HashMap<>();
        for (String rewardKey : section.getConfigurationSection("items").getKeys(false)) {
            CrateReward reward = new CrateReward(crateKey, rewardKey);
            this.itemList.add(reward.getChance(), reward);
            this.chanceMap.put(reward, reward.getChance());
            this.rewardList.add(reward);
        }
        this.borderItem = new ItemBuilder(ItemUtils.getInstance().getMaterial(section.getString("settings.border.material")), 0).setName(section.getString("settings.border.name")).setLore(section.getStringList("settings.border.lore")).setAmount(section.getInt("settings.border.amount")).getStack();
        this.crateItem = new ItemBuilder(ItemUtils.getInstance().getMaterial(section.getString("settings.crate.material")), ItemUtils.getInstance().getData(section.getString("settings.crate.material"))).setName(section.getString("settings.crate.name")).setLore(section.getStringList("settings.crate.lore")).getStack();
        this.prizeItem = new ItemBuilder(ItemUtils.getInstance().getMaterial(section.getString("prizeItem.material")), section.getInt("prizeItem.data")).setName(section.getString("prizeItem.name")).setLore(section.getStringList("prizeItem.lore")).setAmount(section.getInt("prizeItem.amount")).getStack();
        if (section.getBoolean("settings.border.enchanted")) {
            EnchantGlow.addGlow(this.borderItem);
        }
        if (section.getBoolean("prizeItem.enchanted")) {
            EnchantGlow.addGlow(this.prizeItem);
        }
        if (section.getBoolean("settings.crate.enchanted")) {
            EnchantGlow.addGlow(this.crateItem);
        }
        this.borderDataList = section.getIntegerList("settings.border.dataList");
        this.previewTitle = section.getString("settings.preview");
        System.out.print("[C0d3rCrates] Loaded Crate \"" + crateKey + "\"!");
    }

    public double getWorth() {
        return worth;
    }

    public void setWorth(double worth) {
        this.worth = worth;
    }

    public int getMaxRewardsReverseCSGOSelect() {
        return maxRewardsReverseCSGOSelect;
    }

    public void setMaxRewardsReverseCSGOSelect(int maxRewardsReverseCSGOSelect) {
        this.maxRewardsReverseCSGOSelect = maxRewardsReverseCSGOSelect;
    }

    public Boolean getAmountPlayer() {
        return amountPlayer;
    }

    public void setAmountPlayer(Boolean amountPlayer) {
        this.amountPlayer = amountPlayer;
    }

    public int getMaxRewardsInstant() {
        return maxRewardsInstant;
    }

    public void setMaxRewardsInstant(int maxRewardsInstant) {
        this.maxRewardsInstant = maxRewardsInstant;
    }

    public int getMaxRewards3BY3() {
        return maxRewards3BY3;
    }

    public void setMaxRewards3BY3(int maxRewards3BY3) {
        this.maxRewards3BY3 = maxRewards3BY3;
    }

    public Boolean getHologramEnabled() {
        return hologramEnabled;
    }

    public void setHologramEnabled(Boolean hologramEnabled) {
        this.hologramEnabled = hologramEnabled;
    }

    public List<CrateReward> getRewardList() {
        return rewardList;
    }

    public void setRewardList(List<CrateReward> rewardList) {
        this.rewardList = rewardList;
    }

    public boolean isAnimationEnabled(Animations animation) {
        return Main.getInstance().getConfig().getBoolean("crates." + getCrateKey() + ".settings.animations.animations." + animation.getAnimationName());
    }

    public Boolean getPresetAnimation() {
        return presetAnimation;
    }

    public void setPresetAnimation(Boolean presetAnimation) {
        this.presetAnimation = presetAnimation;
    }

    public Animations getAnimation() {
        return animation;
    }

    public void setAnimation(Animations animation) {
        this.animation = animation;
    }

    public String getHologramFormat() {
        return hologramFormat;
    }

    public void setHologramFormat(String hologramFormat) {
        this.hologramFormat = hologramFormat;
    }

    public int getMaxRewardsCSGOSelect() {
        return maxRewardsCSGOSelect;
    }

    public void setMaxRewardsCSGOSelect(int maxRewardsCSGOSelect) {
        this.maxRewardsCSGOSelect = maxRewardsCSGOSelect;
    }

    public String getParticleName() {
        return particleName;
    }

    public void setParticleName(String particleName) {
        this.particleName = particleName;
    }

    public String getPreviewTitle() {
        return previewTitle;
    }

    public void setPreviewTitle(String previewTitle) {
        this.previewTitle = previewTitle;
    }

    public ItemStack getCrateItem() {
        return crateItem;
    }

    public void setCrateItem(ItemStack crateItem) {
        this.crateItem = crateItem;
    }

    public ItemStack getPrizeItem() {
        return prizeItem;
    }

    public void setPrizeItem(ItemStack prizeItem) {
        this.prizeItem = prizeItem;
    }

    public List<Integer> getBorderDataList() {
        return borderDataList;
    }

    public void setBorderDataList(List<Integer> borderDataList) {
        this.borderDataList = borderDataList;
    }

    public ItemStack getBorderItem() {
        return borderItem;
    }

    public void setBorderItem(ItemStack borderItem) {
        this.borderItem = borderItem;
    }

    public CrateReward pickReward() {
        return itemList.next();
    }

    public int getSlot() {
        return slot;
    }

    public void setSlot(int slot) {
        this.slot = slot;
    }

    public String getCrateKey() {
        return crateKey;
    }

    public void setCrateKey(String crateKey) {
        this.crateKey = crateKey;
    }

    public ItemStack getPhysicalKey() {
        return physicalKey;
    }

    public void setPhysicalKey(ItemStack physicalKey) {
        this.physicalKey = physicalKey;
    }

    public ItemStack getMenuItem() {
        if (dataIterator != null) {
            if (!dataIterator.hasNext()) {
                dataIterator = dataList.iterator();
            }
            ItemStack item = new ItemStack(menuItem);
            item.setDurability(dataIterator.next().shortValue());
            return item;
        }
        return new ItemStack(menuItem);
    }

    public void setMenuItem(ItemStack menuItem) {
        this.menuItem = menuItem;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public RandomPick<CrateReward> getItemList() {
        return itemList;
    }

    public void setItemList(RandomPick<CrateReward> itemList) {
        this.itemList = itemList;
    }

    public CrateReward rewardFromItemStack(ItemStack i) {
        for (CrateReward rewards : this.rewardList) {
            if (ItemUtils.getInstance().isSimilar(i, rewards.getItem())) {
                return rewards;
            }
        }
        return null;
    }

    public Map<CrateReward, Double> getChanceMap() {
        return chanceMap;
    }

}