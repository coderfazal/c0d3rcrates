package org.c0d3r.crates.crate;

import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.inventory.ItemStack;
import org.c0d3r.crates.Main;
import org.c0d3r.crates.utils.EnchantGlow;
import org.c0d3r.crates.utils.ItemBuilder;
import org.c0d3r.crates.utils.Utils;

import java.util.List;
import java.util.Objects;

public class CrateReward {

    private ItemStack item;

    private Double chance;

    private List<String> commandList;

    private String rewardKey;
    private String hologramText;

    private Crate parent;

    public CrateReward(String crateKey, String rewardKey) {
        ConfigurationSection section = Main.getInstance().getConfig().getConfigurationSection("crates." + crateKey + ".items." + rewardKey);
        String materialName = section.getString("displayItem.material");
        int data = section.getInt("displayItem.data");
        int amount = section.getInt("displayItem.amount");
        String name = section.getString("displayItem.name");
        String skull = section.getString("skull");
        List<String> lore = section.getStringList("displayItem.lore");
        Double chance = section.getDouble("chance");
        List<String> commandList = section.getStringList("commands");
        Material material = Material.getMaterial(materialName);
        if (material == null) {
            if (Utils.isInteger(materialName)) {
                material = Material.getMaterial(Integer.parseInt(materialName));
            } else if (Objects.equals(materialName, "MOB_SPAWNER")) {
                material = Material.MOB_SPAWNER;
            } else if (materialName.contains(";")) {
                String[] materialArgs = materialName.trim().split(";");
                if (Utils.isInteger(materialArgs[0])) {
                    material = Material.getMaterial(Integer.parseInt(materialArgs[0]));
                }
                if (Utils.isInteger(materialArgs[1])) {
                    data = Integer.parseInt(materialArgs[1]);
                }
            } else {
                try {
                    material = Main.getInstance().getEss().getItemDb().get(materialName, 1).getType();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        ItemBuilder builder = new ItemBuilder(material, data);
        builder.setAmount(amount);
        builder.setName(name);
        builder.setLore(lore);
        if (skull != null) {
            builder.setOwner(skull);
        }
        this.chance = chance;
        this.rewardKey = rewardKey;
        this.item = builder.getStack();
        this.commandList = commandList;
        if (section.getBoolean("displayItem.enchanted")) {
            EnchantGlow.addGlow(this.item);
        }
        this.hologramText = Main.getInstance().getConfig().getString("crates." + crateKey + ".settings.hologram.format").replace("%reward%", rewardKey);
        System.out.print("[C0d3rCrates] Loaded Crate Reward \"" + rewardKey + "\" for Crate \"" + crateKey + "\"");
    }

    public String getHologramText() {
        return hologramText;
    }

    public void setHologramText(String hologramText) {
        this.hologramText = hologramText;
    }

    public String getRewardKey() {
        return rewardKey;
    }

    public void setRewardKey(String rewardKey) {
        this.rewardKey = rewardKey;
    }

    public Crate getParent() {
        return parent;
    }

    public void setParent(Crate parent) {
        this.parent = parent;
    }

    public ItemStack getItem() {
        return item;
    }

    public void setItem(ItemStack item) {
        this.item = item;
    }

    public Double getChance() {
        return chance;
    }

    public void setChance(Double chance) {
        this.chance = chance;
    }

    public List<String> getCommandList() {
        return commandList;
    }

    public void setCommandList(List<String> commandList) {
        this.commandList = commandList;
    }
}
