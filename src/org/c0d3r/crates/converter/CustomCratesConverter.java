package org.c0d3r.crates.converter;

import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.c0d3r.crates.Main;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;

public class CustomCratesConverter implements Listener {

    private static CustomCratesConverter instance;
    private File conversionFilef;
    private FileConfiguration conversionFile = new YamlConfiguration();
    private boolean converting = false;
    private String player;

    public static CustomCratesConverter getInstance() {
        if (CustomCratesConverter.instance == null) {
            synchronized (CustomCratesConverter.class) {
                if (CustomCratesConverter.instance == null) {
                    CustomCratesConverter.instance = new CustomCratesConverter();
                }
            }
        }
        return CustomCratesConverter.instance;
    }

    public void convertConfigs(Player player) {
        if (converting) {
            player.sendMessage("§cA config is already being converted please wait.");
            return;
        }
        this.converting = true;
        this.player = player.getName();
        conversionFilef = new File(Main.getInstance().getDataFolder(), "conversionFile.yml");
        conversionFile = new YamlConfiguration();
        try {
            conversionFilef.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        player.sendMessage("§aA conversionFile.yml has been created in the C0d3rCrates directory paste your CustomCrates config.yml there to be converted. Once completed type in chat §l'done'");
    }

    private void convertReward(ConfigurationSection section, ConfigurationSection crateConfig) {
        int index = 0;
        for (String rewardIndex : section.getKeys(false)) {
            ConfigurationSection ccConfig = section.getConfigurationSection(rewardIndex);
            crateConfig.set("items." + index + ".displayItem.material", ccConfig.getString("material"));
            crateConfig.set("items." + index + ".displayItem.enchanted", ccConfig.getBoolean("enchanted"));
            crateConfig.set("items." + index + ".displayItem.amount", ccConfig.getInt("amount"));
            crateConfig.set("items." + index + ".displayItem.data", 0);
            crateConfig.set("items." + index + ".displayItem.name", ccConfig.getString("name"));
            crateConfig.set("items." + index + ".displayItem.lore", Collections.singletonList("&7"));
            crateConfig.set("items." + index + ".chance", ccConfig.getDouble("chance"));
            crateConfig.set("items." + index + ".commands", ccConfig.getList("commands"));
            index++;
        }
    }

    private void convertCrates(ConfigurationSection section) {
        int index = 1;
        FileConfiguration config = Main.getInstance().getConfig();
        for (String crateName : section.getKeys(false)) {
            ConfigurationSection ccConfig = section.getConfigurationSection(crateName);
            config.set("crates." + crateName + ".settings.hologram.enabled", true);
            config.set("crates." + crateName + ".settings.hologram.format", "&a&l[!] &b%reward%");
            config.set("crates." + crateName + ".settings.hologram.info", Arrays.asList("&e&l" + crateName + " Crate &6[Tier " + index + "]", "&7Click to open menu."));
            config.set("crates." + crateName + ".settings.preview", ccConfig.getString("PreviewInv.name").replace("%crate%", crateName));
            config.set("crates." + crateName + ".settings.particle", "REDSTONE");
            config.set("crates." + crateName + ".settings.border.material", ccConfig.getString("Border.material"));
            config.set("crates." + crateName + ".settings.border.amount", ccConfig.getInt("Border.amount"));
            config.set("crates." + crateName + ".settings.border.enchanted", ccConfig.getBoolean("Border.enchanted"));
            config.set("crates." + crateName + ".settings.border.name", ccConfig.getString("Border.name"));
            config.set("crates." + crateName + ".settings.border.lore", Collections.singletonList("&7"));
            config.set("crates." + crateName + ".settings.border.dataList", ccConfig.getIntegerList("Border.datas"));
            config.set("crates." + crateName + ".settings.crate.material", "CHEST");
            config.set("crates." + crateName + ".settings.crate.enchanted", false);
            config.set("crates." + crateName + ".settings.crate.name", "&7Crate &8* &a" + crateName);
            config.set("crates." + crateName + ".settings.crate.lore", Arrays.asList("&7", "&7&o(( &f&oTip: &7&oPlace to register this crate ))"));
            config.set("crates." + crateName + ".settings.animations.presetAnimation", false);
            config.set("crates." + crateName + ".settings.animations.presetAnimationName", "SHRINK");
            config.set("crates." + crateName + ".settings.animations.animations.CSGO", true);
            config.set("crates." + crateName + ".settings.animations.animations.REVERSECSGO", true);
            config.set("crates." + crateName + ".settings.animations.animations.ROULETTE", true);
            config.set("crates." + crateName + ".settings.animations.animations.WHEELOFFORTUNE", true);
            config.set("crates." + crateName + ".settings.animations.animations.SHRINK", true);
            config.set("crates." + crateName + ".settings.animations.animations.INSTANT", true);
            config.set("crates." + crateName + ".settings.animations.animations.DOUBLECSGO", false);
            config.set("crates." + crateName + ".settings.animations.animations.3BY3", true);
            config.set("crates." + crateName + ".settings.animations.animations.SLOT", false);
            config.set("crates." + crateName + ".settings.animations.animations.CSGO-SELECT", true);
            config.set("crates." + crateName + ".settings.animations.animations.REVERSE-CSGO-SELECT", true);
            config.set("crates." + crateName + ".settings.animations.animations.PHYSICAL", true);
            config.set("crates." + crateName + ".settings.animations.settings.3BY3.rewards", 1);
            config.set("crates." + crateName + ".settings.animations.settings.CSGO-SELECT.rewards", 1);
            config.set("crates." + crateName + ".settings.animations.settings.INSTANT.rewards", 1);
            config.set("crates." + crateName + ".settings.animations.settings.REVERSE-CSGO-SELECT.rewards", 1);
            config.set("crates." + crateName + ".title", ccConfig.getString("OpenInv.name").replace("%crate%", crateName));
            config.set("crates." + crateName + ".slot", ccConfig.getInt("Key.slot", index));
            config.set("crates." + crateName + ".globalKey.material", "DIAMOND");
            config.set("crates." + crateName + ".globalKey.enchanted", false);
            config.set("crates." + crateName + ".globalKey.data", 0);
            config.set("crates." + crateName + ".globalKey.name", "&b" + crateName + " Crate");
            config.set("crates." + crateName + ".globalKey.lore", Arrays.asList("", " &3&l* &bCrate: &f" + crateName, " &3&l* &bTier: &f" + index, "", "&7&o(( &f&oTip: &7&oRight-Click to open a " + crateName + " Crate ))"));
            config.set("crates." + crateName + ".displayItem.material", ccConfig.getString("Key.material"));
            config.set("crates." + crateName + ".displayItem.data", ccConfig.getInt("Key.data"));
            config.set("crates." + crateName + ".displayItem.amount", 1);
            config.set("crates." + crateName + ".displayItem.enchanted", ccConfig.getBoolean("Key.enchanted"));
            config.set("crates." + crateName + ".displayItem.name", ccConfig.getString("Key.name"));
            config.set("crates." + crateName + ".displayItem.lore", ccConfig.getStringList("Key.lore"));
            config.set("crates." + crateName + ".prizeItem.material", ccConfig.getString("PrizeItem.material"));
            config.set("crates." + crateName + ".prizeItem.amount", 1);
            config.set("crates." + crateName + ".prizeItem.data", 0);
            config.set("crates." + crateName + ".prizeItem.enchanted", ccConfig.getBoolean("PrizeItem.enchanted"));
            config.set("crates." + crateName + ".prizeItem.name", ccConfig.getString("PrizeItem.name"));
            config.set("crates." + crateName + ".prizeItem.lore", Collections.singletonList(""));
            this.convertReward(ccConfig.getConfigurationSection("Items"), config.getConfigurationSection("crates." + crateName));
            index++;
        }
        Main.getInstance().saveConfig();
        Main.getInstance().reloadConfig();
    }

    private void convert() {
        Player player = Bukkit.getPlayer(this.player);
        if (player != null) {
            player.sendMessage("§aStarting conversion.");
        }
        try {
            this.conversionFile.load(this.conversionFilef);
        } catch (IOException | InvalidConfigurationException e) {
            e.printStackTrace();
        }
        long startTime = System.currentTimeMillis();
        this.convertCrates(this.conversionFile.getConfigurationSection("Crates"));
        if (player != null) {
            player.sendMessage("§aThe CustomCrates file has been successfully converted §7(" + (System.currentTimeMillis() - startTime) + "ms)");
        }
        if (this.conversionFilef.delete()) {
            System.out.print("[C0d3rCrates] conversionFile.yml has been deleted");
        } else System.out.print("[C0d3rCrates] conversionFile.yml failed to delete.");
        this.conversionFile = null;
        this.converting = false;
        this.player = null;
        this.conversionFilef = null;
        Bukkit.getScheduler().scheduleSyncDelayedTask(Main.getInstance(), () -> {
            Main.getInstance().getCrates().reload();
            Main.getInstance().getCrates().deleteHolograms();
            Main.getInstance().getCrates().deleteParticles();
            Main.getInstance().getCrates().reInitLocations();
        }, 100L);
    }

    @EventHandler
    public void onChat(AsyncPlayerChatEvent e) {
        if (converting) {
            if (e.getPlayer().getName().equals(player)) {
                if (e.getMessage().equalsIgnoreCase("done")) {
                    e.setCancelled(true);
                    this.convert();
                }
            }
        }
    }

}