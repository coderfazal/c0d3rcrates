package org.c0d3r.crates.tabcomplete;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class CratesTabComplete implements TabCompleter {

    private final List<String> subCommands = Arrays.asList("help", "open");

    public CratesTabComplete() {
        Collections.sort(subCommands);
    }

    public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
        if (cmd.getName().equalsIgnoreCase("crates")) {
            if (args.length == 1) {
                if (!args[0].isEmpty()) {
                    String argument = args[0].toLowerCase();
                    List<String> foundSubCommands = new ArrayList<>();
                    for (String command : subCommands) {
                        if (command.toLowerCase().startsWith(argument)) {
                            foundSubCommands.add(command);
                        }
                    }
                    Collections.sort(foundSubCommands);
                    return foundSubCommands;
                }
                return subCommands;
            }
        }
        return null;
    }

}
