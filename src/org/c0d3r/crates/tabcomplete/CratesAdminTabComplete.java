package org.c0d3r.crates.tabcomplete;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.c0d3r.crates.Main;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class CratesAdminTabComplete implements TabCompleter {

    private final List<String> subCommands = Arrays.asList("convert", "help", "give", "givephys", "remove", "set", "giveall", "givecrate", "reload", "save");
    private final List<String> amounts = Arrays.asList("1", "2", "4", "8", "16", "32", "64");

    public CratesAdminTabComplete() {
        Collections.sort(subCommands);
    }

    public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
        if (cmd.getName().equalsIgnoreCase("cratesadmin")) {
            if (args.length == 1) {
                if (!args[0].isEmpty()) {
                    String argument = args[0].toLowerCase();
                    List<String> foundSubCommands = new ArrayList<>();
                    for (String command : subCommands) {
                        if (command.toLowerCase().startsWith(argument)) {
                            foundSubCommands.add(command);
                        }
                    }
                    Collections.sort(foundSubCommands);
                    return foundSubCommands;
                }
                return subCommands;
            } else if (args.length == 2) {
                if (args[0].equalsIgnoreCase("giveall") || args[0].equalsIgnoreCase("givecrate")) {
                    List<String> crateList = Main.getInstance().getCrates().getCrateNameList();
                    if (!args[1].isEmpty()) {
                        String argument = args[1].toLowerCase();
                        List<String> foundSubCommands = new ArrayList<>();
                        for (String crate : crateList) {
                            if (crate.toLowerCase().startsWith(argument)) {
                                foundSubCommands.add(crate);
                            }
                        }
                        Collections.sort(foundSubCommands);
                        return foundSubCommands;
                    }
                    Collections.sort(crateList);
                    return crateList;
                }
            } else if (args.length == 3) {
                if (args[0].equalsIgnoreCase("give") || args[0].equalsIgnoreCase("givephys") || args[0].equalsIgnoreCase("remove") || args[0].equalsIgnoreCase("set")) {
                    List<String> crateList = Main.getInstance().getCrates().getCrateNameList();
                    if (!args[2].isEmpty()) {
                        String argument = args[2].toLowerCase();
                        List<String> foundSubCommands = new ArrayList<>();
                        for (String crate : crateList) {
                            if (crate.toLowerCase().startsWith(argument)) {
                                foundSubCommands.add(crate);
                            }
                        }
                        Collections.sort(foundSubCommands);
                        return foundSubCommands;
                    }
                    Collections.sort(crateList);
                    return crateList;
                } else if (args[0].equalsIgnoreCase("giveall")) {
                    return this.amounts;
                }
            } else if (args.length == 4 && (args[0].equalsIgnoreCase("give") || args[0].equalsIgnoreCase("givephys") || args[0].equalsIgnoreCase("remove") || args[0].equalsIgnoreCase("set")) && args[3].isEmpty()) {
                return this.amounts;
            }
        }
        return null;
    }

}
