package org.c0d3r.crates.menus;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.c0d3r.crates.Main;

public abstract class MenuItem {
    private Menu menu;
    private int slot;
    private boolean clickable = true;

    void addToMenu(final Menu menu) {
        this.menu = menu;
    }

    void removeFromMenu(final Menu menu) {
        if (this.menu == menu) {
            this.menu = null;
        }
    }

    public Menu getMenu() {
        return this.menu;
    }

    public int getSlot() {
        return this.slot;
    }

    public void setSlot(final int slot) {
        this.slot = slot;
    }

    public abstract void onClick(final Player p0, final InventoryClickType p1);

    public abstract ItemStack getItemStack();

    public void setTemporaryIcon(ItemStack newItem, long ticks) {
        clickable = false;
        ItemStack item = getItemStack();
        menu.getInventory().setItem(getSlot(), newItem);
        Bukkit.getScheduler().runTaskLater(Main.getInstance(), () -> {
            clickable = true;
            menu.getInventory().setItem(getSlot(), item);
        }, ticks);
    }

    public abstract static class UnclickableMenuItem extends MenuItem {
        public void onClick() {
        }
    }

    public boolean isClickable() {
        return clickable;
    }

    public void setClickable(boolean clickable) {
        this.clickable = clickable;
    }
}
