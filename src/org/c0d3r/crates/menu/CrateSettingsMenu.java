package org.c0d3r.crates.menu;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.c0d3r.crates.Main;
import org.c0d3r.crates.data.DataPlayer;
import org.c0d3r.crates.menus.InventoryClickType;
import org.c0d3r.crates.menus.Menu;
import org.c0d3r.crates.menus.MenuAPI;
import org.c0d3r.crates.menus.MenuItem;
import org.c0d3r.crates.utils.EnchantGlow;
import org.c0d3r.crates.utils.ItemUtils;
import org.c0d3r.crates.utils.SoundUtils;
import org.c0d3r.crates.utils.Utils;

public class CrateSettingsMenu {

    public CrateSettingsMenu(Player player, DataPlayer dataPlayer) {
        Menu menu = MenuAPI.getInstance().createMenu(Utils.toColor(Main.getInstance().getConfig().getString("menus.crateSettings.title", "&7Crate Settings")), Main.getInstance().getConfig().getInt("menus.crateSettings.size", 9) / 9);
        menu.addMenuItem(new MenuItem() {
            @Override
            public void onClick(Player player, InventoryClickType clickType) {
                dataPlayer.setAutoInstant(!dataPlayer.isAutoInstant());
                new SoundUtils(player).playSound("ANIMATION_TOGGLE");
                menu.updateMenu();
            }

            @Override
            public ItemStack getItemStack() {
                boolean enabled = !dataPlayer.isAutoInstant();
                ItemStack item = ItemUtils.getInstance().loadItem("menus.crateSettings.items.autoInstant");
                ItemUtils.getInstance().parsePlaceholders(item, new String[]{"%color%", "%toggle%", "%toggleupper%"}, new String[]{Main.getInstance().getConfig().getString("menus.crateSettings." + (enabled ? "enabledColor" : "disabledColor")), enabled ? "disable" : "enable", enabled ? "ENABLE" : "DISABLE"});
                if (enabled) {
                    EnchantGlow.addGlow(item);
                }
                return item;
            }
        }, Main.getInstance().getConfig().getInt("menus.crateSettings.items.autoInstant.slot", 3));
        menu.addMenuItem(new MenuItem() {
            @Override
            public void onClick(Player player, InventoryClickType clickType) {
                menu.setBypassMenuCloseBehaviour(true);
                menu.closeMenu(player);
                Bukkit.getScheduler().runTaskLater(Main.getInstance(), () -> {
                    new AmountSettingsMenu(player, dataPlayer);
                    new SoundUtils(player).playSound("SHIFT_CLICK_SETTINGS_OPEN");
                }, 5L);
            }

            @Override
            public ItemStack getItemStack() {
                ItemStack item = ItemUtils.getInstance().loadItem("menus.crateSettings.items.shiftClickAmount");
                ItemUtils.getInstance().parsePlaceholders(item, new String[]{"%amount%"}, new String[]{dataPlayer.getShiftClickAmount() + ""});
                return item;
            }
        }, Main.getInstance().getConfig().getInt("menus.crateSettings.items.shiftClickAmount.slot", 5));
        for (int free = 0; free < menu.getInventory().getSize(); ++free) {
            if (menu.getInventory().getItem(free) == null) {
                final MenuItem.UnclickableMenuItem item = new MenuItem.UnclickableMenuItem() {
                    @Override
                    public void onClick(Player p0, InventoryClickType p1) {

                    }

                    @Override
                    public ItemStack getItemStack() {
                        return ItemUtils.getInstance().getUnusedItem();
                    }
                };
                menu.addMenuItem(item, free);
            }
        }
        menu.setMenuCloseBehaviour((player1, menu1, bypass) -> Bukkit.getScheduler().runTaskLater(Main.getInstance(), () -> {
            if (!bypass) new CrateMenu(player, dataPlayer);
        }, 5L));
        menu.openMenu(player);
    }

}
