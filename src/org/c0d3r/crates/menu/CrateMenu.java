package org.c0d3r.crates.menu;

import com.gmail.filoghost.holographicdisplays.api.HologramsAPI;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.c0d3r.crates.Main;
import org.c0d3r.crates.animation.Animation;
import org.c0d3r.crates.animation.Animations;
import org.c0d3r.crates.animation.handler.AnimationHandler;
import org.c0d3r.crates.animation.player.AnimationManager;
import org.c0d3r.crates.animation.player.AnimationPlayer;
import org.c0d3r.crates.animation.pretask.CSGOSelectSlotPick;
import org.c0d3r.crates.animation.pretask.ReverseCSGOSelectSlotPick;
import org.c0d3r.crates.animation.pretask.ThreeByThreeSlotPick;
import org.c0d3r.crates.animation.settings.AnimationSettings;
import org.c0d3r.crates.animation.types.*;
import org.c0d3r.crates.api.events.CrateOpenEvent;
import org.c0d3r.crates.api.events.CratePreOpenEvent;
import org.c0d3r.crates.crate.Crate;
import org.c0d3r.crates.data.DataManager;
import org.c0d3r.crates.data.DataPlayer;
import org.c0d3r.crates.menus.InventoryClickType;
import org.c0d3r.crates.menus.Menu;
import org.c0d3r.crates.menus.MenuItem;
import org.c0d3r.crates.utils.ItemUtils;
import org.c0d3r.crates.utils.MessageUtils;
import org.c0d3r.crates.utils.Utils;

public class CrateMenu {

    public CrateMenu(Player player, DataPlayer dataPlayer) {
        Menu menu = new Menu(Utils.toColor(Main.getInstance().getConfig().getString("menus.mainMenu.title")), Main.getInstance().getConfig().getInt("menus.mainMenu.size") / 9);
        for (int index = 0; index < Main.getInstance().getCrates().getCrateList().size(); index++) {
            Crate crate = Main.getInstance().getCrates().getCrateList().get(index);
            MenuItem icon = new MenuItem() {
                @Override
                public void onClick(Player player, InventoryClickType clickType) {
                    DataPlayer dataPlayer = DataManager.getInstance().getByPlayer(player);
                    if (dataPlayer == null) return;
                    if (clickType.isShiftClick()) {
                        if (!Main.getInstance().getConfig().getBoolean("menus.crateSettings.enabled")) {
                            return;
                        }
                        if (dataPlayer.getKey(Main.getInstance().getCrates().crateToString(crate)) < dataPlayer.getShiftClickAmount()) {
                            MessageUtils.getInstance().sendMessageList(player, "NOT_ENOUGH_KEYS", "%crate%", crate.getCrateKey());
                            setTemporaryIcon(ItemUtils.getInstance().loadItem("menus.mainMenu.temporaryIcons.notEnoughKeys"), 100L);
                            player.updateInventory();
                            return;
                        }
                        dataPlayer.removeKeys(Main.getInstance().getCrates().crateToString(crate), dataPlayer.getShiftClickAmount());
                        AnimationPlayer animationPlayer = AnimationManager.createAnimationPlayer(player, crate);
                        animationPlayer.add();
                        animationPlayer.setAnimations(Animations.INSTANT);
                        Animation animation = new InstantAnimation(player, crate, dataPlayer.getShiftClickAmount());
                        animation.start();
                        for (int i = 0; i < dataPlayer.getShiftClickAmount(); i++) {
                            CrateOpenEvent.fireEvent(player, crate.getCrateKey());
                        }
                    } else if (clickType.isLeftClick()) {
                        if (!Utils.canUse(player)) {
                            return;
                        }
                        if (dataPlayer.getKey(Main.getInstance().getCrates().crateToString(crate)) < 1) {
                            MessageUtils.getInstance().sendMessageList(player, "NOT_ENOUGH_KEYS", "%crate%", crate.getCrateKey());
                            setTemporaryIcon(ItemUtils.getInstance().loadItem("menus.mainMenu.temporaryIcons.notEnoughKeys"), 100L);
                            player.updateInventory();
                            return;
                        }
                        if (AnimationManager.hasAnimationPlayer(player)) {
                            player.updateInventory();
                            menu.closeMenu(player);
                            return;
                        }
                        dataPlayer.removeKeys(Main.getInstance().getCrates().crateToString(crate), 1);
                        AnimationPlayer animationPlayer = AnimationManager.createAnimationPlayer(player, crate);
                        animationPlayer.add();
                        if (Main.getInstance().getConfig().getBoolean("menus.crateSettings.enabled") && dataPlayer.isAutoInstant() && crate.isAnimationEnabled(Animations.INSTANT)) {
                            animationPlayer.setAnimations(Animations.INSTANT);
                            Animation animation = new InstantAnimation(player, crate);
                            CratePreOpenEvent.fireEvent(player, crate, animation);
                            return;
                        }
                        animationPlayer.setAnimations(crate.getAnimation());
                        if (crate.getPresetAnimation()) {
                            Animations animations = crate.getAnimation();
                            Animation animation = null;
                            Inventory crateInventory;
                            if (animations == Animations.CSGO) {
                                crateInventory = Bukkit.createInventory(null, animations.getInvSize(), Utils.toColor(crate.getTitle()));
                                AnimationSettings animationSettings = new AnimationSettings();
                                animation = new CSGOAnimation(player, crate, crateInventory, animationSettings);
                            } else if (animations == Animations.INSTANT) {
                                animation = new InstantAnimation(player, crate);
                            } else if (animations == Animations.REVERSECSGO) {
                                crateInventory = Bukkit.createInventory(null, animations.getInvSize(), Utils.toColor(crate.getTitle()));
                                AnimationSettings animationSettings = new AnimationSettings();
                                animation = new ReverseCSGOAnimation(player, crate, crateInventory, animationSettings);
                            } else if (animations == Animations.ROULETTE) {
                                crateInventory = Bukkit.createInventory(null, animations.getInvSize(), Utils.toColor(crate.getTitle()));
                                AnimationSettings animationSettings = new AnimationSettings();
                                animation = new RouletteAnimation(player, crate, crateInventory, animationSettings);
                            } else if (animations == Animations.WHEELOFFORTUNE) {
                                crateInventory = Bukkit.createInventory(null, animations.getInvSize(), Utils.toColor(crate.getTitle()));
                                AnimationSettings animationSettings = new AnimationSettings();
                                animation = new WOFAnimation(player, crate, crateInventory, animationSettings);
                            } else if (animations == Animations.SHRINK) {
                                crateInventory = Bukkit.createInventory(null, animations.getInvSize(), Utils.toColor(crate.getTitle()));
                                animation = new ShrinkAnimation(player, crate, crateInventory);
                            } else if (animations == Animations.DOUBLECSGO) {
                                AnimationSettings animationSettings = new AnimationSettings();
                                crateInventory = Bukkit.createInventory(null, animations.getInvSize(), Utils.toColor(crate.getTitle()));
                                animation = new DoubleCSGOAnimation(player, crate, crateInventory, animationSettings);
                            } else if (animations == Animations.THREEBYTHREE) {
                                menu.setBypassMenuCloseBehaviour(true);
                                menu.closeMenu(player);
                                animationPlayer.setSlotTitle(Utils.toColor(Main.getInstance().getConfig().getString("menus.slotPicker.title").replace("%slots%", crate.getMaxRewards3BY3() + "")));
                                ThreeByThreeSlotPick.getInstance().openSlotPicker(player);
                                return;
                            } else if (animations == Animations.SLOT) {
                                crateInventory = Bukkit.createInventory(null, animations.getInvSize(), Utils.toColor(crate.getTitle()));
                                AnimationSettings animationSettings = new AnimationSettings();
                                animation = new SlotAnimation(player, crate, crateInventory, animationSettings);
                            } else if (animations == Animations.CSGOSELECT) {
                                menu.setBypassMenuCloseBehaviour(true);
                                menu.closeMenu(player);
                                animationPlayer.setSlotTitle(Utils.toColor(Main.getInstance().getConfig().getString("menus.slotPicker.title").replace("%slots%", crate.getMaxRewardsCSGOSelect() + "")));
                                CSGOSelectSlotPick.getInstance().openSlotPicker(player);
                                return;
                            } else if (animations == Animations.REVERSECSGOSELECT) {
                                menu.setBypassMenuCloseBehaviour(true);
                                menu.closeMenu(player);
                                animationPlayer.setSlotTitle(Utils.toColor(Main.getInstance().getConfig().getString("menus.slotPicker.title").replace("%slots%", crate.getMaxRewardsReverseCSGOSelect() + "")));
                                ReverseCSGOSelectSlotPick.getInstance().openSlotPicker(player);
                                return;
                            } else if (animations == Animations.PHYSICAL) {
                                AnimationSettings animationSettings = new AnimationSettings();
                                animation = new PhsyicalAnimation(player, crate, HologramsAPI.createHologram(Main.getInstance(), player.getLocation().clone().add(0.5, 3.33, 0.5)), animationSettings);
                            }
                            if (animation != null) {
                                CratePreOpenEvent.fireEvent(player, crate, animation);
                            }
                        } else {
                            AnimationHandler.getInstance().openAnimationPicker(player);
                        }
                    } else if (clickType.isRightClick()) {
                        if (AnimationManager.hasAnimationPlayer(player)) {
                            player.updateInventory();
                            menu.closeMenu(player);
                            return;
                        }
                        new PreviewMenu(player, crate, menu);
                    }
                }

                @Override
                public ItemStack getItemStack() {
                    return ItemUtils.getInstance().getCrateKey(crate, dataPlayer.getKey(crate.getCrateKey()), dataPlayer.getShiftClickAmount());
                }
            };
            menu.addMenuItem(icon, crate.getSlot());
        }
        if (Main.getInstance().getConfig().getBoolean("menus.crateSettings.enabled")) {
            menu.addMenuItem(new MenuItem() {
                @Override
                public void onClick(Player player, InventoryClickType clickType) {
                    new CrateSettingsMenu(player, dataPlayer);
                }

                @Override
                public ItemStack getItemStack() {
                    return ItemUtils.getInstance().loadItem("menus.crateSettings.items.crateItem");
                }
            }, Main.getInstance().getConfig().getInt("menus.crateSettings.slot", 26));
        }
        for (int free = 0; free < menu.getInventory().getSize(); ++free) {
            if (menu.getInventory().getItem(free) == null) {
                final MenuItem.UnclickableMenuItem item = new MenuItem.UnclickableMenuItem() {
                    @Override
                    public void onClick(Player player, InventoryClickType p1) {
                    }

                    @Override
                    public ItemStack getItemStack() {
                        return ItemUtils.getInstance().getUnusedItem();
                    }
                };
                menu.addMenuItem(item, free);
            }
        }
        menu.openMenu(player);
        menu.updateMenu();
        if (Main.getInstance().getConfig().getInt("menus.mainMenu.update") > 0) {
            menu.scheduleUpdateTask(player, Main.getInstance().getConfig().getInt("menus.mainMenu.update"));
        }
    }

}
