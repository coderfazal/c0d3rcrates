package org.c0d3r.crates.menu;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.c0d3r.crates.Main;
import org.c0d3r.crates.data.DataPlayer;
import org.c0d3r.crates.menus.InventoryClickType;
import org.c0d3r.crates.menus.Menu;
import org.c0d3r.crates.menus.MenuAPI;
import org.c0d3r.crates.menus.MenuItem;
import org.c0d3r.crates.utils.ItemUtils;
import org.c0d3r.crates.utils.SoundUtils;
import org.c0d3r.crates.utils.Utils;

public class AmountSettingsMenu {

    public AmountSettingsMenu(Player player, DataPlayer dataPlayer) {
        Menu menu = MenuAPI.getInstance().createMenu(Utils.toColor(Main.getInstance().getConfig().getString("menus.amountSettings.title", "&7Amount Settings")), Main.getInstance().getConfig().getInt("menus.amountSettings.size", 9) / 9);
        for (int amount = 0; amount < menu.getInventory().getSize(); amount++) {
            int finalAmount = amount + 1;
            menu.addMenuItem(new MenuItem() {
                @Override
                public void onClick(Player player, InventoryClickType clickType) {
                    dataPlayer.setShiftClickAmount(finalAmount);
                    new SoundUtils(player).playSound("SHIFT_CLICK_CHANGE");
                    setTemporaryIcon(ItemUtils.getInstance().loadItem("menus.amountSettings.temporaryItem"), 100L);
                }

                @Override
                public ItemStack getItemStack() {
                    ItemStack item = ItemUtils.getInstance().loadItem("menus.amountSettings.item");
                    ItemUtils.getInstance().parsePlaceholders(item, new String[]{"%amount%", "%plural%"}, new String[]{finalAmount + "", finalAmount == 1 ? "" : "s"});
                    return item;
                }
            }, amount);
        }
        menu.setMenuCloseBehaviour((player1, menu1, bypass) -> Bukkit.getScheduler().runTaskLater(Main.getInstance(), () -> {
            if (!bypass) new CrateSettingsMenu(player, dataPlayer);
        }, 5L));
        menu.openMenu(player);
    }

}
