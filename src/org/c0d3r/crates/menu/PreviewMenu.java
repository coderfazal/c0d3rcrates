package org.c0d3r.crates.menu;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.c0d3r.crates.Main;
import org.c0d3r.crates.crate.Crate;
import org.c0d3r.crates.crate.CrateReward;
import org.c0d3r.crates.menus.InventoryClickType;
import org.c0d3r.crates.menus.Menu;
import org.c0d3r.crates.menus.MenuAPI;
import org.c0d3r.crates.menus.MenuItem;
import org.c0d3r.crates.utils.ItemBuilder;
import org.c0d3r.crates.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class PreviewMenu {

    public PreviewMenu(Player player, Crate crate, Menu menu) {
        Menu previewMenu = MenuAPI.getInstance().createMenu(Utils.toColor(crate.getPreviewTitle()), Utils.inventoryRowCalculator(crate.getItemList().size()));
        previewMenu.setParent(menu);
        previewMenu.setMenuCloseBehaviour((player1, menu1, bypass) -> {
            if (!bypass) {
                Bukkit.getScheduler().scheduleSyncDelayedTask(Main.getInstance(), () -> {
                    if (!menu1.bypassMenuCloseBehaviour() && menu1.getParent() != null) {
                        menu1.getParent().openMenu(player1);
                        if (Main.getInstance().getConfig().getInt("menus.mainMenu.update") > 0) {
                            menu1.scheduleUpdateTask(player1, Main.getInstance().getConfig().getInt("menus.mainMenu.update"));
                        }
                    }
                }, 5L);
            }
        });
        if (crate.getItemList().getMap().values().size() > 45) {
            List<MenuItem> items = new ArrayList<>();
            for (CrateReward reward : crate.getItemList().getMap().values()) {
                ItemStack itemStack = reward.getItem();
                items.add(new MenuItem() {
                    @Override
                    public void onClick(Player player, InventoryClickType clickType) {
                        player.updateInventory();
                        player.closeInventory();
                    }

                    @Override
                    public ItemStack getItemStack() {
                        return itemStack;
                    }
                });
            }
            previewMenu.addMenuItem(new MenuItem() {
                @Override
                public void onClick(Player player1, InventoryClickType clickType) {
                    previewMenu.previousPage(player1);
                }

                @Override
                public ItemStack getItemStack() {
                    return new ItemBuilder(Material.getMaterial(Main.getInstance().getConfig().getString("items.preview-menu.previous.material")), Main.getInstance().getConfig().getInt("items.preview-menu.previous.data")).setName(Main.getInstance().getConfig().getString("items.preview-menu.previous.name")).setLore(Main.getInstance().getConfig().getStringList("items.preview-menu.previous.lore")).getStack();
                }
            }, previewMenu.getInventory().getSize() - 6);
            previewMenu.addMenuItem(new MenuItem() {
                @Override
                public void onClick(Player player1, InventoryClickType clickType) {
                    previewMenu.nextPage(player1);
                }

                @Override
                public ItemStack getItemStack() {
                    return new ItemBuilder(Material.getMaterial(Main.getInstance().getConfig().getString("items.preview-menu.next.material")), Main.getInstance().getConfig().getInt("items.preview-menu.next.data")).setName(Main.getInstance().getConfig().getString("items.preview-menu.next.name")).setLore(Main.getInstance().getConfig().getStringList("items.preview-menu.next.lore")).getStack();
                }
            }, previewMenu.getInventory().getSize() - 4);
            List<Integer> scrollSlots = new ArrayList<>();
            for (int i = 0; i < previewMenu.getInventory().getSize() - 9; i++) {
                scrollSlots.add(i);
            }
            previewMenu.setupPages(items, scrollSlots);
        } else {
            int slot = 0;
            for (CrateReward reward : crate.getItemList().getMap().values()) {
                ItemStack itemStack = reward.getItem();
                MenuItem icon = new MenuItem() {
                    @Override
                    public void onClick(Player player, InventoryClickType clickType) {
                        player.updateInventory();
                        player.closeInventory();
                    }

                    @Override
                    public ItemStack getItemStack() {
                        return itemStack;
                    }
                };
                previewMenu.addMenuItem(icon, slot);
                slot++;
            }
        }
        previewMenu.addMenuItem(new MenuItem() {
            @Override
            public void onClick(Player p0, InventoryClickType p1) {
                p0.updateInventory();
                p0.closeInventory();
            }

            @Override
            public ItemStack getItemStack() {
                return new ItemBuilder(Material.getMaterial(Main.getInstance().getConfig().getString("items.preview-menu.mainmenu.material")), Main.getInstance().getConfig().getInt("items.preview-menu.mainmenu.data")).setName(Main.getInstance().getConfig().getString("items.preview-menu.mainmenu.name")).setLore(Main.getInstance().getConfig().getStringList("items.preview-menu.mainmenu.lore")).getStack();
            }
        }, previewMenu.getInventory().getSize() - 5);
        menu.closeMenu(player);
        previewMenu.openMenu(player);
        Bukkit.getScheduler().scheduleSyncDelayedTask(Main.getInstance(), player::updateInventory, 20L);
    }

}
