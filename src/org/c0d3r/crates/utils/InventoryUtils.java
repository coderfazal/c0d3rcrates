package org.c0d3r.crates.utils;

import org.bukkit.inventory.Inventory;

public class InventoryUtils {

    public static void fillInventory(Inventory inv) {
        for (int i = 0; i < inv.getSize(); i++) {
            if (inv.getItem(i) == null) {
                inv.setItem(i, ItemUtils.getInstance().getUnusedItem());
            }
        }
    }

}
