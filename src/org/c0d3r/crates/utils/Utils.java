package org.c0d3r.crates.utils;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.c0d3r.crates.Main;
import org.c0d3r.crates.animation.player.AnimationManager;

import java.util.concurrent.TimeUnit;

public class Utils {

    public static boolean isOverloaded(Player player) {
        if (Main.getInstance().getConfig().getBoolean("overload.enabled", true) && AnimationManager.getOpeningAmount() >= Main.getInstance().getConfig().getInt("overload.maxCratesAtOneTime", 50)) {
            MessageUtils.getInstance().sendMessageList(player, "OVERLOADED");
            return true;
        }
        return false;
    }

    public static boolean canUse(Player player) {
        return !isOverloaded(player) && !alreadyOpening(player);
    }

    public static boolean alreadyOpening(Player player) {
        return AnimationManager.hasAnimationPlayer(player);
    }

    public static boolean isInteger(String s) {
        try {
            Integer.parseInt(s);
        } catch (NumberFormatException | NullPointerException e) {
            return false;
        }
        return true;
    }

    public static String toColor(String s) {
        return ChatColor.translateAlternateColorCodes('&', s);
    }

    public static String formatTime(final Long timeLeft) {
        final int hours = (int) TimeUnit.MILLISECONDS.toHours(timeLeft);
        final int minute = (int) (TimeUnit.MILLISECONDS.toMinutes(timeLeft) - TimeUnit.HOURS.toMinutes(hours));
        final int second = (int) (TimeUnit.MILLISECONDS.toSeconds(timeLeft) - (TimeUnit.HOURS.toSeconds(hours) + TimeUnit.MINUTES.toSeconds(minute)));
        return String.format("%02d hours %02d minutes %02d seconds", hours, minute, second);
    }

    public static int inventoryRowCalculator(final int slots) {
        if (slots <= 9) {
            return 2;
        }
        if (slots <= 18) {
            return 3;
        }
        if (slots <= 27) {
            return 4;
        }
        if (slots <= 36) {
            return 5;
        }
        if (slots <= 45) {
            return 6;
        }
        return 6;
    }

}
