package org.c0d3r.crates.utils;

import java.util.NavigableMap;
import java.util.Random;
import java.util.TreeMap;

public class RandomPick<E> {
    private NavigableMap<Double, E> map;
    private Random random;
    private double total;

    public RandomPick() {
        this(new Random());
    }

    private RandomPick(Random random) {
        this.map = new TreeMap<>();
        this.total = 0.0;
        this.random = random;
    }

    public void add(double weight, E result) {
        if (weight <= 0.0) {
            return;
        }
        this.total += weight;
        this.map.put(this.total, result);
    }

    public NavigableMap<Double, E> getMap() {
        return map;
    }

    public E next() {
        double value = this.random.nextDouble() * this.total;
        return this.map.ceilingEntry(value).getValue();
    }

    public int size() {
        return this.map.size();
    }

    public void destroy() {
        this.random = null;
        this.map.clear();
        this.map = null;
        this.total = 0.0;
    }
}

