package org.c0d3r.crates.utils;

import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.c0d3r.crates.Main;
import org.c0d3r.crates.crate.Crate;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static org.c0d3r.crates.utils.Utils.isInteger;
import static org.c0d3r.crates.utils.Utils.toColor;

public class ItemUtils {

    private static ItemUtils instance;

    public static ItemUtils getInstance() {
        if (instance == null) {
            instance = new ItemUtils();
        }
        return instance;
    }

    public Material getMaterial(String materialName) {
        Material material = Material.getMaterial(materialName);
        if (material == null) {
            if (isInteger(materialName)) {
                material = Material.getMaterial(Integer.parseInt(materialName));
            } else if (Objects.equals(materialName, "MOB_SPAWNER")) {
                material = Material.MOB_SPAWNER;
            } else if (materialName.contains(";")) {
                String[] materialArgs = materialName.trim().split(";");
                if (isInteger(materialArgs[0])) {
                    material = Material.getMaterial(Integer.parseInt(materialArgs[0]));
                }
            } else if (materialName.contains(":")) {
                String[] materialArgs = materialName.trim().split(":");
                if (isInteger(materialArgs[0])) {
                    material = Material.getMaterial(Integer.parseInt(materialArgs[0]));
                }
            } else if (materialName.contains(",")) {
                String[] materialArgs = materialName.trim().split(",");
                if (isInteger(materialArgs[0])) {
                    material = Material.getMaterial(Integer.parseInt(materialArgs[0]));
                }
            } else if (Main.getInstance().getEss() != null) {
                try {
                    material = Main.getInstance().getEss().getItemDb().get(materialName, 1).getType();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return material;
    }

    public int getData(String materialName) {
        Material material = Material.getMaterial(materialName);
        int data = 0;
        if (material == null) {
            if (materialName.contains(";")) {
                String[] materialArgs = materialName.trim().split(";");
                if (isInteger(materialArgs[1])) {
                    data = Integer.parseInt(materialArgs[1]);
                }
            } else if (materialName.contains(":")) {
                String[] materialArgs = materialName.trim().split(":");
                if (isInteger(materialArgs[1])) {
                    data = Integer.parseInt(materialArgs[1]);
                }
            } else if (materialName.contains(",")) {
                String[] materialArgs = materialName.trim().split(",");
                if (isInteger(materialArgs[1])) {
                    data = Integer.parseInt(materialArgs[1]);
                }
            }
        }
        return data;
    }

    public ItemStack getCrateKey(Crate crate, int amount, int shiftAmount) {
        ItemStack itemStack = crate.getMenuItem();
        if (crate.getAmountPlayer()) {
            itemStack.setAmount((amount < 1) ? 1 : Math.min(amount, 64));
        }
        ItemMeta itemMeta = itemStack.getItemMeta();
        if (itemMeta.hasLore()) {
            List<String> lore = itemMeta.getLore();
            for (int i = 0; i < lore.size(); i++) {
                String line = lore.get(i);
                line = line.replace("%amount%", Integer.toString(amount));
                line = line.replace("%shift-amount%", Integer.toString(shiftAmount));
                lore.set(i, line);
            }
            itemMeta.setLore(lore);
            itemStack.setItemMeta(itemMeta);
        }
        return itemStack;
    }

    public ItemStack loadItem(String path) {
        return loadItem(path, null);
    }

    public ItemStack loadItem(String path, Player player) {
        ItemStack itemStack;
        ConfigurationSection section = Main.getInstance().getConfig().getConfigurationSection(path);
        if (section == null) {
            System.out.print("[C0d3rCrates] \"" + path + "\" path does not exist");
            return null;
        }
        String materialName = section.getString("material", "STONE");
        Material material = Material.getMaterial(materialName);
        int data = 0;
        String name = section.getString("name");
        name = toColor(name);
        List<String> lore = new ArrayList<>();
        section.getStringList("lore").stream().map(Utils::toColor).forEach(lore::add);
        if (material == null) {
            if (isInteger(materialName)) {
                material = Material.getMaterial(Integer.parseInt(materialName));
            } else if (materialName.contains(";")) {
                String[] materialArgs = materialName.trim().split(";");
                if (isInteger(materialArgs[0])) {
                    material = Material.getMaterial(Integer.parseInt(materialArgs[0]));
                    data = Integer.parseInt(materialArgs[1]);
                }
            } else if (materialName.contains(":")) {
                String[] materialArgs = materialName.trim().split(":");
                if (isInteger(materialArgs[0])) {
                    material = Material.getMaterial(Integer.parseInt(materialArgs[0]));
                    data = Integer.parseInt(materialArgs[1]);
                }
            } else if (materialName.contains(",")) {
                String[] materialArgs = materialName.trim().split(",");
                if (isInteger(materialArgs[0])) {
                    material = Material.getMaterial(Integer.parseInt(materialArgs[0]));
                    data = Integer.parseInt(materialArgs[1]);
                }
            } else if (Main.getInstance().getEss() != null) {
                try {
                    material = Main.getInstance().getEss().getItemDb().get(materialName, 1).getType();
                } catch (Exception ignore) {
                }
            }
            if (material == null) {
                material = Material.STONE;
                System.out.print("[C0d3rCrates] Unable to load material \"" + materialName + "\" for path \"" + path + "\", so it has been set to STONE.");
            }
        }
        if (data == 0) {
            data = section.getInt("data", 0);
        }
        itemStack = new ItemStack(material, 1);
        itemStack.setDurability((short) data);
        ItemMeta meta = itemStack.getItemMeta();
        if (name != null) {
            meta.setDisplayName(name);
        }
        if (!lore.isEmpty()) {
            meta.setLore(lore);
        }
        if (itemStack.getType() == Material.SKULL_ITEM && itemStack.getDurability() == (short) 3 && section.getString("owner") != null) {
            if (player != null && player.getName() != null && section.getString("owner").equalsIgnoreCase("%player%")) {
                ((SkullMeta) meta).setOwner(player.getName());
            } else {
                ((SkullMeta) meta).setOwner(section.getString("owner", "Fazal"));
            }
        }
        itemStack.setAmount(section.getInt("amount", 1));
        itemStack.setItemMeta(meta);
        return itemStack;
    }

    public void parsePlaceholders(ItemStack item, String[] replace, String[] replacement) {
        if (replace.length == replacement.length) {
            ItemMeta meta = item.getItemMeta();
            String displayname = meta.getDisplayName();
            List<String> lore = meta.getLore();
            for (int i = 0; i < replace.length; i++) {
                displayname = displayname.replace(replace[i], replacement[i]);
            }
            for (int i = 0; i < lore.size(); i++) {
                String line = lore.get(i);
                for (int i1 = 0; i1 < replace.length; i1++) {
                    line = line.replaceAll(replace[i1], replacement[i1]);
                }
                line = toColor(line);
                lore.set(i, line);
            }
            displayname = toColor(displayname);
            meta.setDisplayName(displayname);
            meta.setLore(lore);
            item.setItemMeta(meta);
        }
    }

    public ItemStack getUnusedItem() {
        return new ItemBuilder(Material.getMaterial(Main.getInstance().getConfig().getString("items.unused-item.material")), Main.getInstance().getConfig().getInt("items.unused-item.data")).setName(Main.getInstance().getConfig().getString("items.unused-item.name")).setLore(Main.getInstance().getConfig().getStringList("items.unused-item.lore")).getStack();
    }

    public ItemStack getUnPickedItem() {
        return new ItemBuilder(Material.getMaterial(Main.getInstance().getConfig().getString("items.unpicked-slot.material")), Main.getInstance().getConfig().getInt("items.unpicked-slot.data")).setName(Main.getInstance().getConfig().getString("items.unpicked-slot.name")).setLore(Main.getInstance().getConfig().getStringList("items.unpicked-slot.lore")).getStack();
    }

    public ItemStack getPickedItem() {
        return new ItemBuilder(Material.getMaterial(Main.getInstance().getConfig().getString("items.picked-slot.material")), Main.getInstance().getConfig().getInt("items.picked-slot.data")).setName(Main.getInstance().getConfig().getString("items.picked-slot.name")).setLore(Main.getInstance().getConfig().getStringList("items.picked-slot.lore")).getStack();
    }

    public ItemStack getAnimationItem(String name) {
        ConfigurationSection section = Main.getInstance().getConfig().getConfigurationSection("animations." + name);
        if (section.getBoolean("enchanted")) {
            return new ItemBuilder(Material.getMaterial(section.getString("material")), section.getInt("data")).setName(section.getString("name")).setLore(section.getStringList("lore")).glow().getStack();
        }
        return new ItemBuilder(Material.getMaterial(section.getString("material")), section.getInt("data")).setName(section.getString("name")).setLore(section.getStringList("lore")).getStack();
    }

    public boolean isSimilar(final ItemStack item, final ItemStack compare) {
        if (item == null || compare == null) {
            return false;
        }
        if (item == compare) {
            return true;
        }
        if (item.getTypeId() != compare.getTypeId()) {
            return false;
        }
        if (item.getDurability() != compare.getDurability()) {
            return false;
        }
        if (item.hasItemMeta() != compare.hasItemMeta()) {
            return false;
        }
        if (item.hasItemMeta() && item.getItemMeta().hasDisplayName()) {
            if (item.getItemMeta().hasDisplayName() != compare.getItemMeta().hasDisplayName()) {
                return false;
            }
            return item.getItemMeta().getDisplayName().equals(compare.getItemMeta().getDisplayName());
        }
        return true;
    }

}
