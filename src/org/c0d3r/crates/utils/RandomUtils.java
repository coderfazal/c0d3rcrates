package org.c0d3r.crates.utils;

import java.util.Random;

class RandomUtils {

    public static int getRandomInteger(int lower, int upper) {
        Random random = new Random();
        return random.nextInt((upper - lower) + 1) + lower;
    }

}
