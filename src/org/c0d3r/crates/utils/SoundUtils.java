package org.c0d3r.crates.utils;

import org.bukkit.entity.Player;
import org.c0d3r.crates.Main;

public class SoundUtils {

    private Player player;

    public SoundUtils(Player player) {
        this.player = player;
    }

    public void playSound(String configName) {
        if (Main.getInstance().getConfig().getBoolean("sounds." + configName + ".enabled")) {
            player.playSound(player.getLocation(), Sounds.valueOf(Main.getInstance().getConfig().getString("sounds." + configName + ".sound").toUpperCase()).bukkitSound(), 1.0F, 1.5F);
        }
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }
}
