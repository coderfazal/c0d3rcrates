package org.c0d3r.crates.utils;

import org.bukkit.Bukkit;
import org.bukkit.Location;

import java.text.DecimalFormat;

public class LocationUtils {

    public static String locationToString(Location loc) {
        DecimalFormat mat = new DecimalFormat("###");
        return loc.getWorld().getName() + ";" + mat.format(loc.getX()) + ";" + mat.format(loc.getY()) + ";" + mat.format(loc.getZ());
    }

    public static Location stringtoLocation(String str) {
        String[] parts = str.split(";");
        return new Location(Bukkit.getWorld(parts[0]), (double) Integer.valueOf(parts[1]), (double) Integer.valueOf(parts[2]), (double) Integer.valueOf(parts[3]));
    }

}
