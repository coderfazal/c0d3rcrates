package org.c0d3r.crates.utils.hologram;

import com.gmail.filoghost.holographicdisplays.api.Hologram;
import com.gmail.filoghost.holographicdisplays.api.HologramsAPI;
import com.gmail.filoghost.holographicdisplays.api.line.ItemLine;
import com.gmail.filoghost.holographicdisplays.api.line.TextLine;
import org.bukkit.Location;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;
import org.c0d3r.crates.Main;
import org.c0d3r.crates.crate.CrateReward;
import org.c0d3r.crates.utils.Utils;

import java.util.Iterator;
import java.util.List;

public class HologramsEffect extends BukkitRunnable {

    private final Location baseLocation;
    private final Location location;
    private final List<CrateReward> rewards;
    private final String crate;
    private final String format;
    private Iterator<CrateReward> remainingRewards;
    private Hologram rewardHologram;

    public HologramsEffect(final String crate, final Location baseLocation, String format) {
        this.crate = crate;
        this.location = baseLocation.clone();
        this.format = Utils.toColor(format);
        this.baseLocation = baseLocation.add(0.5, 0.5, 0.5);
        this.rewards = Main.getInstance().getCrates().stringToCrate(crate).getRewardList();
        this.remainingRewards = null;
        this.setupHologram();
    }

    public void cancel() {
        this.rewardHologram.delete();
        super.cancel();
    }

    public void run() {
        this.updateHologram();
    }

    private void setupHologram() {
        this.rewardHologram = HologramsAPI.createHologram(Main.getInstance(), this.baseLocation.add(0.0, 1.5, 0.0));
        final ItemStack reward = this.getNextReward();
        if (reward != null) {
            if (reward.hasItemMeta() && reward.getItemMeta().hasDisplayName()) {
                this.rewardHologram.appendTextLine(format.replace("%reward%", reward.getItemMeta().getDisplayName()));
            }
            this.rewardHologram.appendItemLine(reward);
        }
    }

    private void updateHologram() {
        int itemIndex = 0;
        final ItemStack reward = this.getNextReward();
        if (reward != null) {
            if (reward.hasItemMeta() && reward.getItemMeta().hasDisplayName()) {
                itemIndex = 1;
                ((TextLine) this.rewardHologram.getLine(0)).setText(format.replace("%reward%", reward.getItemMeta().getDisplayName()));
            }
            ((ItemLine) this.rewardHologram.getLine(itemIndex)).setItemStack(reward);
        }
    }

    private ItemStack getNextReward() {
        if (this.remainingRewards == null || !this.remainingRewards.hasNext()) {
            this.remainingRewards = this.rewards.iterator();
        }
        return this.remainingRewards.next().getItem();
    }

    public String getCrate() {
        return this.crate;
    }

    public Location getLocation() {
        return this.location;
    }
}