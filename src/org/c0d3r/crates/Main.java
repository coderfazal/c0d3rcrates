package org.c0d3r.crates;

import com.earth2me.essentials.Essentials;
import com.google.common.collect.Lists;
import org.bukkit.Bukkit;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;
import org.c0d3r.crates.animation.handler.AnimationHandler;
import org.c0d3r.crates.animation.player.AnimationManager;
import org.c0d3r.crates.animation.pretask.CSGOSelectSlotPick;
import org.c0d3r.crates.animation.pretask.ReverseCSGOSelectSlotPick;
import org.c0d3r.crates.animation.pretask.ThreeByThreeSlotPick;
import org.c0d3r.crates.commands.CratesAdminCommand;
import org.c0d3r.crates.commands.CratesCommand;
import org.c0d3r.crates.converter.CustomCratesConverter;
import org.c0d3r.crates.crate.Crates;
import org.c0d3r.crates.data.DataManager;
import org.c0d3r.crates.listeners.*;
import org.c0d3r.crates.menus.MenuAPI;
import org.c0d3r.crates.placeholders.PAPIHook;
import org.c0d3r.crates.tabcomplete.CratesAdminTabComplete;
import org.c0d3r.crates.tabcomplete.CratesTabComplete;
import org.c0d3r.crates.tasks.SaveTask;
import org.c0d3r.crates.update.UpdateCheck;
import org.c0d3r.crates.webserver.LicenseValidation;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;

public class Main extends JavaPlugin {

    private static Main instance;
    private Crates crates;
    private File crateLocationsf;
    private FileConfiguration crateLocations;
    private File dataf;
    private FileConfiguration data;
    private UpdateCheck update;
    private Essentials ess;
    private SaveTask saveThread;

    public static Main getInstance() {
        return instance;
    }

    public void onEnable() {
        saveDefaultConfig();
        setupCrateLocations();
        setupData();
        instance = this;
        this.ess = (Essentials) Bukkit.getPluginManager().getPlugin("Essentials");
        int configVersion = 6;
        if (!getConfig().isInt("config-version") || getConfig().getInt("config-version", 0) < configVersion) {
            if (getConfig().getInt("config-version") == 1) {
                if (getConfig().getConfigurationSection("crates") != null) {
                    for (String crateKey : getConfig().getConfigurationSection("crates").getKeys(false)) {
                        getConfig().set("crates." + crateKey + ".settings.animations.settings.3BY3.rewards", 2);
                        getConfig().set("crates." + crateKey + ".settings.animations.settings.CSGO-SELECT.rewards", 2);
                    }
                    getConfig().set("config-version", 2);
                    saveConfig();
                    reloadConfig();
                }
            }
            if (getConfig().getInt("config-version") == 2) {
                if (getConfig().getConfigurationSection("crates") != null) {
                    for (String crateKey : getConfig().getConfigurationSection("crates").getKeys(false)) {
                        getConfig().set("crates." + crateKey + ".settings.animations.settings.INSTANT.rewards", 1);
                    }
                }
                getConfig().set("save-database-in-minutes", 5);
                getConfig().set("messages.SAVE_TASK_START", Collections.singletonList("&7&o[C0d3rCrates] Starting save task!"));
                getConfig().set("messages.SAVE_TASK_DONE", Collections.singletonList("&7&o[C0d3rCrates] Save task has been finished! (%ms%ms)"));
                getConfig().set("config-version", 3);
                saveConfig();
                reloadConfig();
            }
            if (getConfig().getInt("config-version") == 3) {
                if (getConfig().getConfigurationSection("crates") != null) {
                    int tier = 1;
                    for (String crateKey : getConfig().getConfigurationSection("crates").getKeys(false)) {
                        getConfig().set("crates." + crateKey + ".settings.hologram.info", Arrays.asList("&e&l" + crateKey + " Crate &6[Tier " + tier + "]", "&7Click to open menu."));
                        tier++;
                    }
                }
                getConfig().set("config-version", 4);
                saveConfig();
                reloadConfig();
            }
            if (getConfig().getInt("config-version") == 4) {
                if (getConfig().getConfigurationSection("crates") != null) {
                    for (String crateKey : getConfig().getConfigurationSection("crates").getKeys(false)) {
                        getConfig().set("crates." + crateKey + ".settings.animations.settings.REVERSE-CSGO-SELECT.rewards", 1);
                        getConfig().set("crates." + crateKey + ".settings.animations.animations.REVERSE-CSGO-SELECT", true);
                        getConfig().set("crates." + crateKey + ".settings.animations.animations.PHYSICAL", true);
                    }
                }
                getConfig().set("overload.enabled", true);
                getConfig().set("overload.maxCratesAtOneTime", 50);
                getConfig().set("config-version", 5);
                saveConfig();
                reloadConfig();
            }
            if (getConfig().getInt("config-version") == 5) {
                if (getConfig().getConfigurationSection("crates") != null) {
                    for (String crateKey : getConfig().getConfigurationSection("crates").getKeys(false)) {
                        String particleName = getConfig().getString("crates." + crateKey + ".particle");
                        if (particleName != null) {
                            if (particleName.equalsIgnoreCase("LAVA_POP")) {
                                particleName = "REDSTONE";
                            }
                            getConfig().set("crates." + crateKey + ".particle", null);
                            getConfig().set("crates." + crateKey + ".settings.particle", particleName);
                        }
                        String particleToCorrect = getConfig().getString("crates." + crateKey + ".settings.particle");
                        if (particleName.equalsIgnoreCase("LAVA_POP")) {
                            particleToCorrect = "REDSTONE";
                        }
                        getConfig().set("crates." + crateKey + ".settings.particle", particleToCorrect);
                    }
                }
                getConfig().set("config-version", 6);
                saveConfig();
                reloadConfig();
            }
            if (getConfig().getInt("config-version") < 8) {
                getConfig().set("mode", "virtual");
                getConfig().set("messages.VIRTUAL_DISABLED", Lists.newArrayList("&6&oC0d3rCrates &8* &7Virtual crates are currently disabled..."));
                getConfig().set("messages.NEED_KEY_TO_OPEN", Lists.newArrayList("&6&oC0d3rCrates &8* &7You need a key to open this crate!"));
                getConfig().set("messages.WRONG_KEY", Lists.newArrayList("&6&oC0d3rCrates &8* &7You are using the wrong key to open this crate!"));
                getConfig().set("config-version", 8);
                saveConfig();
                reloadConfig();
            }
            if (getConfig().getInt("config-version") < 9) {
                getConfig().set("preview-left-click-crate-block", true);
                getConfig().set("config-version", 9);
                saveConfig();
                reloadConfig();
            }
        }
        DataManager.getInstance().register();
        this.crates = new Crates();
        loadListeners(new CrateListener(), new JoinListener(), new BreakListener(), CSGOSelectSlotPick.getInstance(), ThreeByThreeSlotPick.getInstance(), CustomCratesConverter.getInstance(), DataManager.getInstance(), MenuAPI.getInstance(), new AnimationManager(), AnimationHandler.getInstance(), new PlaceListener(), new InteractListener(), ReverseCSGOSelectSlotPick.getInstance());
        if (getConfig().getString("license") == null || getConfig().getString("license").equalsIgnoreCase("XXXX-XXXX-XXXX-XXXX")) {
            System.out.print("[C0d3rCrates] \"license\" option in config is either null or default.");
            System.out.print("[C0d3rCrates] Therefore the plugin will be disabled.");
            Bukkit.getScheduler().scheduleSyncDelayedTask(this, () -> getServer().getPluginManager().disablePlugin(Main.getInstance()), 50L);
        } else {
            if (!new LicenseValidation(getConfig().getString("license"), "https://www.viraxis.net/license/verify.php", this).register())
                return;
        }
        getCommand("cratesadmin").setExecutor(new CratesAdminCommand());
        System.out.print("[C0d3rCrates] Registered \"CratesAdminCommand.class\" for command \"/cratesadmin\"");
        getCommand("crates").setExecutor(new CratesCommand());
        System.out.print("[C0d3rCrates] Registered \"CratesCommand.class\" for command \"/crates\"");
        getCommand("cratesadmin").setTabCompleter(new CratesAdminTabComplete());
        System.out.print("[C0d3rCrates] Registered \"CratesAdminTabComplete.class\" as a tab completer for command \"/cratesadmin\"!");
        getCommand("crates").setTabCompleter(new CratesTabComplete());
        System.out.print("[C0d3rCrates] Registered \"CratesTabComplete.class\" as a tab completer for command \"/crates\"!");
        this.crates.initLocations();
        if (getConfig().getBoolean("updateCheck")) {
            update = new UpdateCheck("https://www.viraxis.net/plugins/versions/codercrates.txt", getDescription().getVersion());
            update.checkUpdate();
        }
        System.out.print("[C0d3rCrates] has been enabled");
        saveThread = new SaveTask();
        saveThread.start();
        if (new File(getDataFolder(), "/database/crateDatabase.db").exists()) {
            System.out.print("[C0d3rCrates] IMPORTANT : C0D3RCRATES HAS NOW OFFICIALLY SWITCHED TO YAML SAVING AND THEREFORE WILL NOT USING ANY OF THE PREVIOUS DATABASE DATA.");
        }
        if (Bukkit.getPluginManager().isPluginEnabled("PlaceholderAPI")) {
            new PAPIHook().register();
        }
    }

    public void onDisable() {
        DataManager.getInstance().unregister();
        Bukkit.getScheduler().cancelTasks(this);
        getCrates().deleteHolograms();
        getCrates().deleteParticles();
        crates.destroy();
        ess = null;
        this.crates = null;
        for (Player player : Bukkit.getOnlinePlayers()) {
            if (AnimationManager.hasAnimationPlayer(player)) {
                AnimationManager.destroyAnimationPlayer(player);
            }
        }
        if (saveThread != null) {
            saveThread.stop();
            saveThread = null;
        }
        System.out.print("[C0d3rCrates] has been disabled");
    }

    public UpdateCheck getUpdate() {
        return update;
    }

    public Crates getCrates() {
        return crates;
    }

    public void setCrates(Crates crates) {
        this.crates = crates;
    }

    public Essentials getEss() {
        return ess;
    }

    public void setEss(Essentials ess) {
        this.ess = ess;
    }

    private void loadListeners(Listener... listeners) {
        for (Listener listener : listeners) {
            getServer().getPluginManager().registerEvents(listener, this);
            System.out.print("[C0d3rCrates] Registered \"" + listener.getClass().getSimpleName() + ".class\" as a listener!");
        }
    }

    public FileConfiguration getData() {
        return data;
    }

    public FileConfiguration getCrateLocations() {
        return crateLocations;
    }

    public void saveCrateLocations() {
        if (crateLocations == null || crateLocationsf == null) return;
        try {
            crateLocations.save(crateLocationsf);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.print("[C0d3rCrates] Failed to save crateLocations.yml");
        }
    }

    public void saveData() {
        if (data == null || dataf == null) return;
        try {
            data.save(dataf);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.print("[C0d3rCrates] Fao;ed tp save data.yml");
        }
    }

    public void reloadCrateLocations() {
        YamlConfiguration.loadConfiguration(crateLocationsf);
    }

    public void reloadData() {
        YamlConfiguration.loadConfiguration(dataf);
    }

    private void setupCrateLocations() {
        crateLocationsf = new File(getDataFolder(), "crateLocations.yml");
        if (!crateLocationsf.exists()) {
            System.out.print("[C0d3rCrates] Config crateLocations.yml does not exist creating!");
            crateLocationsf.getParentFile().mkdirs();
            saveResource("crateLocations.yml", false);
        }
        crateLocations = new YamlConfiguration();
        try {
            crateLocations.load(crateLocationsf);
        } catch (IOException | InvalidConfigurationException e) {
            e.printStackTrace();
            System.out.print("[C0d3rCrates] Failed to load resource crateLocations.yml");
        }
    }

    private void setupData() {
        dataf = new File(getDataFolder(), "data.yml");
        if (!dataf.exists()) {
            System.out.print("[C0d3rCrates] Config data.yml does not exist creating!");
            dataf.getParentFile().mkdirs();
            saveResource("data.yml", false);
        }
        data = new YamlConfiguration();
        try {
            data.load(dataf);
        } catch (IOException | InvalidConfigurationException e) {
            e.printStackTrace();
            System.out.print("[C0d3rCrates Failed to load resource data.yml");
        }
    }


}
//Copyright 2019 FAZAL CODING INDUSTRIES