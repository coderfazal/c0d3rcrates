package org.c0d3r.crates.placeholders;

import me.clip.placeholderapi.expansion.PlaceholderExpansion;
import org.bukkit.entity.Player;
import org.c0d3r.crates.Main;
import org.c0d3r.crates.data.DataManager;
import org.c0d3r.crates.data.DataPlayer;

public class PAPIHook extends PlaceholderExpansion {

    @Override
    public boolean persist() {
        return true;
    }

    @Override
    public boolean canRegister() {
        return true;
    }

    @Override
    public String getAuthor() {
        return Main.getInstance().getDescription().getAuthors().toString();
    }

    @Override
    public String getIdentifier() {
        return "codercrates";
    }

    @Override
    public String getVersion() {
        return Main.getInstance().getDescription().getVersion();
    }

    @Override
    public String onPlaceholderRequest(Player player, String subplaceholder) {
        if (subplaceholder.equals("keys")) {
            String crateName = subplaceholder.replace("keys_", "");
            if (Main.getInstance().getCrates().stringToCrate(crateName) != null) {
                DataPlayer dataPlayer = DataManager.getInstance().getByPlayer(player);
                if (dataPlayer != null) {
                    return dataPlayer.getKey(Main.getInstance().getCrates().stringToCrate(crateName).getCrateKey()) + "";
                }
                return "&4Error: PA3";

            } else return "&4Error: PA2";
        } else return "&4Error: PA1";
    }

}