package org.c0d3r.crates.tasks;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.c0d3r.crates.Main;
import org.c0d3r.crates.data.DataManager;
import org.c0d3r.crates.data.DataPlayer;
import org.c0d3r.crates.utils.MessageUtils;

import java.util.concurrent.TimeUnit;

public class SaveTask extends Thread {

    public SaveTask() {
        System.out.print("[C0d3rCrates] Database save task registered! Save task will run every " + Main.getInstance().getConfig().getInt("save-database-in-minutes") + " minutes!");
    }

    public void run() {
        try {
            long startTime = System.currentTimeMillis();
            for (Player player : Bukkit.getOnlinePlayers()) {
                if (player.isOp() || player.hasPermission("c0d3rcrates.admin")) {
                    MessageUtils.getInstance().sendMessage(player, "SAVE_TASK_START");
                }
            }
            for (DataPlayer data : DataManager.getInstance().getPlayers().values()) {
                data.cleanUp();
            }
            Main.getInstance().saveData();
            Main.getInstance().reloadData();
            for (Player player : Bukkit.getOnlinePlayers()) {
                if (player.isOp() || player.hasPermission("c0d3rcrates.admin")) {
                    MessageUtils.getInstance().sendMessage(player, "SAVE_TASK_DONE", "%ms%", System.currentTimeMillis() - startTime + "");
                }
            }
            Thread.sleep(TimeUnit.MINUTES.toMillis(Main.getInstance().getConfig().getInt("save-database-in-minutes", 120)));
        } catch (InterruptedException e) {
            System.out.print("[C0d3rCrates] There was an error saving data from SaveTask");
            e.printStackTrace();
        }
    }

}
