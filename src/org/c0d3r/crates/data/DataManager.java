package org.c0d3r.crates.data;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.c0d3r.crates.Main;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class DataManager implements Listener {
    private static DataManager instance;
    private Map<String, DataPlayer> players;

    public static DataManager getInstance() {
        if (DataManager.instance == null) {
            synchronized (DataManager.class) {
                if (DataManager.instance == null) {
                    DataManager.instance = new DataManager();
                }
            }
        }
        return DataManager.instance;
    }

    public void register() {
        if (this.players != null) {
            this.players.clear();
        }
        this.players = new HashMap<>();
        for (final Player p : Bukkit.getOnlinePlayers()) {
            DataPlayer data = this.getByPlayer(p);
            if (data == null) {
                data = new DataPlayer(p.getUniqueId());
            }
            data.setup();
            this.players.put(p.getName(), data);
        }
    }

    public void unregister() {
        for (final DataPlayer data : this.players.values()) {
            data.cleanUp();
        }
        Main.getInstance().saveData();
        Main.getInstance().reloadData();
        this.players.clear();
        this.players = null;
    }

    public DataPlayer getByPlayer(final Player p) {
        if (p.getName() == null) {
            return null;
        }
        return players.getOrDefault(p.getName(), null);
    }

    public DataPlayer getByUUID(final UUID uuid) {
        return getByPlayer(Bukkit.getPlayer(uuid));
    }

    @EventHandler
    public void onJoin(final PlayerJoinEvent e) {
        DataPlayer data = this.getByPlayer(e.getPlayer());
        if (data == null) {
            data = new DataPlayer(e.getPlayer().getUniqueId());
            data.setup();
            this.players.put(e.getPlayer().getName(), data);
        }
    }

    public Map<String, DataPlayer> getPlayers() {
        return players;
    }
}