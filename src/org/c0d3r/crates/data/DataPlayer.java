package org.c0d3r.crates.data;

import org.c0d3r.crates.Main;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class DataPlayer {

    private UUID uuid;
    private Map<String, Integer> crateBalance;
    private boolean autoInstant;
    private int shiftClickAmount;

    public DataPlayer(UUID uuid) {
        this.uuid = uuid;
        this.crateBalance = new HashMap<>();
        autoInstant = false;
        shiftClickAmount = 3;
    }

    public void setup() {
        try {
            if (Main.getInstance().getData().getConfigurationSection("data." + getUuid().toString()) != null) {
                for (String crate : Main.getInstance().getData().getConfigurationSection("data." + getUuid().toString()).getKeys(false)) {
                    int amount = Main.getInstance().getData().getInt("data." + getUuid().toString() + "." + crate);
                    getKeys().put(crate, amount);
                }
            }
            if (Main.getInstance().getData().isBoolean("settings." + getUuid().toString() + ".autoInstant")) {
                autoInstant = Main.getInstance().getData().getBoolean("settings." + getUuid().toString() + ".autoInstant", false);
            }
            if (Main.getInstance().getData().isInt("settings." + getUuid().toString() + ".shiftClickAmount")) {
                shiftClickAmount = Main.getInstance().getData().getInt("settings." + getUuid().toString() + ".shiftClickAmount", 3);
            }
        } catch (Exception ignore) {
            Main.getInstance().getLogger().severe("[C0d3rCrates] Failed to get data for " + this.getUuid());
        }
    }

    public void cleanUp() {
        try {
            for (Map.Entry<String, Integer> entry : crateBalance.entrySet()) {
                Main.getInstance().getData().set("data." + getUuid().toString() + "." + entry.getKey(), entry.getValue());
            }
            Main.getInstance().getData().set("settings." + getUuid().toString() + ".autoInstant", autoInstant);
            Main.getInstance().getData().set("settings." + getUuid().toString() + ".shiftClickAmount", shiftClickAmount);
        } catch (Exception ignore) {
            Main.getInstance().getLogger().severe("[C0d3rCrates] Failed to execute update for " + this.getUuid());
        }
    }

    public int getShiftClickAmount() {
        return shiftClickAmount;
    }

    public void setShiftClickAmount(int shiftClickAmount) {
        this.shiftClickAmount = shiftClickAmount;
    }

    public boolean isAutoInstant() {
        return autoInstant;
    }

    public void setAutoInstant(boolean autoInstant) {
        this.autoInstant = autoInstant;
    }

    public int getKey(String crate) {
        if (this.crateBalance.get(crate) != null) {
            return this.crateBalance.get(crate);
        }
        return 0;
    }

    public void giveKeys(String crate, int amount) {
        int exists = this.getKey(crate);
        this.crateBalance.put(crate, exists + amount);
    }

    public void removeKeys(String crate, int amount) {
        int exists = this.getKey(crate);
        this.crateBalance.put(crate, exists - amount);
    }

    public void setKey(String crate, int amount) {
        int exists = this.getKey(crate);
        if (exists < amount) {
            this.giveKeys(crate, amount - exists);
        } else {
            this.removeKeys(crate, exists - amount);
        }
    }

    public UUID getUuid() {
        return this.uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    private Map<String, Integer> getKeys() {
        return this.crateBalance;
    }

    public void setKeys(Map<String, Integer> keys) {
        this.crateBalance = keys;
    }

}