package org.c0d3r.crates.api.events;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class CrateOpenEvent extends Event {

    private static final HandlerList handlers = new HandlerList();

    private Player player;

    private String crate;

    public CrateOpenEvent(Player player, String crate) {
        this.player = player;
        this.crate = crate;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    public static void fireEvent(Player player, String crate) {
        CrateOpenEvent openEvent = new CrateOpenEvent(player, crate);
        Bukkit.getServer().getPluginManager().callEvent(openEvent);
    }

    public Player getPlayer() {
        return player;
    }

    public String getCrate() {
        return crate;
    }

    public HandlerList getHandlers() {
        return handlers;
    }

}
