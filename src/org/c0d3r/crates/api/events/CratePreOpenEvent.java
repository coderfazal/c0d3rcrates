package org.c0d3r.crates.api.events;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.c0d3r.crates.animation.Animation;
import org.c0d3r.crates.crate.Crate;
import org.c0d3r.crates.crate.CrateReward;

import java.util.ArrayList;
import java.util.List;

public class CratePreOpenEvent extends Event implements Cancellable {

    private static final HandlerList handlers = new HandlerList();

    private Player player;

    private Crate crate;

    private Animation animation;

    private List<CrateReward> rewards;

    private boolean cancelled;

    public CratePreOpenEvent(Player player, Crate crate, Animation animation) {
        this.player = player;
        this.crate = crate;
        this.animation = animation;
        this.rewards = new ArrayList<>(crate.getRewardList());
        cancelled = false;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    public static void fireEvent(Player player, Crate crate, Animation animation) {
        CratePreOpenEvent openEvent = new CratePreOpenEvent(player, crate, animation);
        Bukkit.getServer().getPluginManager().callEvent(openEvent);
    }

    public Animation getAnimation() {
        return animation;
    }

    public Player getPlayer() {
        return player;
    }

    public Crate getCrate() {
        return crate;
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public List<CrateReward> getRewards() {
        return rewards;
    }

    @Override
    public boolean isCancelled() {
        return cancelled;
    }

    @Override
    public void setCancelled(boolean b) {
        cancelled = b;
    }
}
