package org.c0d3r.crates.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.c0d3r.crates.Main;
import org.c0d3r.crates.converter.CustomCratesConverter;
import org.c0d3r.crates.crate.Crate;
import org.c0d3r.crates.data.DataManager;
import org.c0d3r.crates.data.DataPlayer;
import org.c0d3r.crates.utils.MessageUtils;
import org.c0d3r.crates.utils.SoundUtils;
import org.c0d3r.crates.utils.Utils;

import java.util.HashMap;
import java.util.Map;

public class CratesAdminCommand implements CommandExecutor {

    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (cmd.getName().equalsIgnoreCase("cratesadmin")) {
            if (args.length == 0) {
                MessageUtils.getInstance().sendMessageList(sender, "CRATE_ADMIN_HELP");
            } else if (args.length == 1) {
                if (args[0].equalsIgnoreCase("help")) {
                    MessageUtils.getInstance().sendMessageList(sender, "CRATE_ADMIN_HELP");
                } else if (args[0].equalsIgnoreCase("convert")) {
                    if (sender instanceof Player) {
                        Player player = (Player) sender;
                        CustomCratesConverter.getInstance().convertConfigs(player);
                    } else sender.sendMessage("§4You must be a player to convert configs!");
                } else if (args[0].equalsIgnoreCase("reload")) {
                    long currentTime = System.currentTimeMillis();
                    Main.getInstance().reloadConfig();
                    Main.getInstance().getCrates().deleteHolograms();
                    Main.getInstance().getCrates().deleteParticles();
                    Main.getInstance().getCrates().reload();
                    MessageUtils.getInstance().sendMessageList(sender, "CRATE_ADMIN_RELOAD", "%time%", String.valueOf(System.currentTimeMillis() - currentTime));
                } else if (args[0].equalsIgnoreCase("save")) {
                    long currentTime = System.currentTimeMillis();
                    DataManager.getInstance().unregister();
                    DataManager.getInstance().register();
                    MessageUtils.getInstance().sendMessageList(sender, "CRATE_ADMIN_SAVE", "%time%", String.valueOf(System.currentTimeMillis() - currentTime));
                } else MessageUtils.getInstance().sendMessageList(sender, "CRATE_ADMIN_USAGE");
            } else if (args.length == 3) {
                if (args[0].equalsIgnoreCase("giveall")) {
                    String crateName = args[1];
                    int amount = Integer.parseInt(args[2]);
                    Crate crate = Main.getInstance().getCrates().stringToCrate(crateName);
                    if (crate != null) {
                        if (Main.getInstance().getConfig().getBoolean("give-all-protection")) {
                            Map<String, String> ips = new HashMap<>();
                            for (Player player : Bukkit.getOnlinePlayers()) {
                                if (!ips.containsKey(player.getAddress().getHostString())) {
                                    player.updateInventory();
                                    for (String message : Main.getInstance().getConfig().getStringList("messages.CRATE_ADMIN_GIVE_KEY")) {
                                        message = Utils.toColor(message);
                                        message = message.replace("%crate%", crate.getCrateKey());
                                        message = message.replace("%amount%", amount + "");
                                        player.sendMessage(message);
                                    }
                                    if (Main.getInstance().getConfig().getString("mode").equalsIgnoreCase("virtual")) {
                                        DataPlayer dataPlayer = DataManager.getInstance().getByPlayer(player);
                                        if (dataPlayer != null) {
                                            dataPlayer.giveKeys(crate.getCrateKey(), amount);
                                        }
                                    } else {
                                        ItemStack physKey = crate.getPhysicalKey();
                                        physKey.setAmount(amount);
                                        player.getInventory().addItem(physKey);
                                        player.updateInventory();
                                    }
                                    SoundUtils soundUtils = new SoundUtils(player);
                                    soundUtils.playSound("KEY_RECEIVE");
                                    ips.put(player.getAddress().getHostString(), player.getName());
                                } else {
                                    MessageUtils.getInstance().sendMessageList(player, "UNIQUE_IP");
                                    System.out.print("[C0d3rCrates] " + player.getName() + " tried to receive extra keys by alting! IP: " + player.getAddress().getHostString());
                                }
                            }
                        } else {
                            for (Player player : Bukkit.getOnlinePlayers()) {
                                player.updateInventory();
                                for (String message : Main.getInstance().getConfig().getStringList("messages.CRATE_ADMIN_GIVE_KEY")) {
                                    message = Utils.toColor(message);
                                    message = message.replace("%crate%", crate.getCrateKey());
                                    message = message.replace("%amount%", amount + "");
                                    player.sendMessage(message);
                                }
                                if (Main.getInstance().getConfig().getString("mode").equalsIgnoreCase("virtual")) {
                                    DataPlayer dataPlayer = DataManager.getInstance().getByPlayer(player);
                                    if (dataPlayer != null) {
                                        dataPlayer.giveKeys(crate.getCrateKey(), amount);
                                    }
                                } else {
                                    ItemStack physKey = crate.getPhysicalKey();
                                    physKey.setAmount(amount);
                                    player.getInventory().addItem(physKey);
                                    player.updateInventory();
                                }
                                SoundUtils soundUtils = new SoundUtils(player);
                                soundUtils.playSound("KEY_RECEIVE");
                            }
                        }
                    } else
                        MessageUtils.getInstance().sendMessageList(sender, "KEY_NOT_FOUND");
                } else if (args[0].equalsIgnoreCase("givecrate")) {
                    String crateName = args[1];
                    String playerName = args[2];
                    Player player = Bukkit.getPlayer(playerName);
                    if (player != null) {
                        Crate crate = Main.getInstance().getCrates().stringToCrate(crateName);
                        if (crate != null) {
                            player.getInventory().addItem(crate.getCrateItem());
                            player.updateInventory();
                            MessageUtils.getInstance().sendMessageList(player, "CRATE_ADMIN_GIVE_CRATEITEM", "%crate%", crate.getCrateKey());
                        } else
                            MessageUtils.getInstance().sendMessageList(sender, "KEY_NOT_FOUND");
                    } else MessageUtils.getInstance().sendMessageList(sender, "PLAYER_NOT_FOUND");
                } else MessageUtils.getInstance().sendMessageList(sender, "CRATE_ADMIN_USAGE");
            } else if (args.length == 4) {
                if (args[0].equalsIgnoreCase("give")) {
                    Player target = Bukkit.getPlayer(args[1]);
                    if (target != null) {
                        String crateName = args[2];
                        Crate crate = Main.getInstance().getCrates().stringToCrate(crateName);
                        if (crate != null) {
                            int amount = Integer.parseInt(args[3]);
                            if (amount > 0) {
                                if (Main.getInstance().getConfig().getString("mode").equalsIgnoreCase("virtual")) {
                                    DataPlayer dataPlayer = DataManager.getInstance().getByPlayer(target);
                                    dataPlayer.giveKeys(crate.getCrateKey(), amount);
                                } else {
                                    ItemStack physKey = crate.getPhysicalKey();
                                    physKey.setAmount(amount);
                                    target.getInventory().addItem(physKey);
                                    target.updateInventory();
                                }
                                SoundUtils soundUtils = new SoundUtils(target);
                                soundUtils.playSound("KEY_RECEIVE");
                                for (String message : Main.getInstance().getConfig().getStringList("messages.CRATE_ADMIN_GIVE_KEY")) {
                                    message = Utils.toColor(message);
                                    message = message.replace("%crate%", crate.getCrateKey());
                                    message = message.replace("%amount%", amount + "");
                                    target.sendMessage(message);
                                }
                            } else MessageUtils.getInstance().sendMessageList(sender, "INVALID_INTEGER");
                        } else
                            MessageUtils.getInstance().sendMessageList(sender, "KEY_NOT_FOUND");
                    } else MessageUtils.getInstance().sendMessageList(sender, "PLAYER_NOT_FOUND");
                } else if (args[0].equalsIgnoreCase("givephys")) {
                    Player target = Bukkit.getPlayer(args[1]);
                    if (target != null) {
                        String crateName = args[2];
                        Crate crate = Main.getInstance().getCrates().stringToCrate(crateName);
                        if (crate != null) {
                            int amount = Integer.parseInt(args[3]);
                            if (amount > 0) {
                                ItemStack physKey = crate.getPhysicalKey();
                                physKey.setAmount(amount);
                                target.getInventory().addItem(physKey);
                                target.updateInventory();
                                SoundUtils soundUtils = new SoundUtils(target);
                                soundUtils.playSound("KEY_RECEIVE");
                                for (String message : Main.getInstance().getConfig().getStringList("messages.CRATE_ADMIN_GIVE_KEY")) {
                                    message = Utils.toColor(message);
                                    message = message.replace("%crate%", crate.getCrateKey());
                                    message = message.replace("%amount%", amount + "");
                                    target.sendMessage(message);
                                }
                            } else MessageUtils.getInstance().sendMessageList(sender, "INVALID_INTEGER");
                        } else
                            MessageUtils.getInstance().sendMessageList(sender, "KEY_NOT_FOUND");
                    } else MessageUtils.getInstance().sendMessageList(sender, "PLAYER_NOT_FOUND");
                } else if (args[0].equalsIgnoreCase("remove")) {
                    Player target = Bukkit.getPlayer(args[1]);
                    if (target != null) {
                        String crateName = args[2];
                        Crate crate = Main.getInstance().getCrates().stringToCrate(crateName);
                        if (crate != null) {
                            int amount = Integer.parseInt(args[3]);
                            if (amount > 0) {
                                DataPlayer dataPlayer = DataManager.getInstance().getByPlayer(target);
                                dataPlayer.removeKeys(crate.getCrateKey(), amount);
                                for (String message : Main.getInstance().getConfig().getStringList("messages.CRATE_ADMIN_REMOVE_KEY")) {
                                    message = Utils.toColor(message);
                                    message = message.replace("%crate%", crate.getCrateKey());
                                    message = message.replace("%amount%", amount + "");
                                    target.sendMessage(message);
                                }
                            } else MessageUtils.getInstance().sendMessageList(sender, "INVALID_INTEGER");
                        } else
                            MessageUtils.getInstance().sendMessageList(sender, "KEY_NOT_FOUND");
                    } else MessageUtils.getInstance().sendMessageList(sender, "PLAYER_NOT_FOUND");
                } else if (args[0].equalsIgnoreCase("set")) {
                    Player target = Bukkit.getPlayer(args[1]);
                    if (target != null) {
                        String crateName = args[2];
                        Crate crate = Main.getInstance().getCrates().stringToCrate(crateName);
                        if (crate != null) {
                            int amount = Integer.parseInt(args[3]);
                            MessageUtils.getInstance().sendMessageList(target, "CRATE_ADMIN_BALANCE_ADJUST");
                            if (amount >= 0) {
                                DataPlayer dataPlayer = DataManager.getInstance().getByPlayer(target);
                                dataPlayer.setKey(crate.getCrateKey(), amount);
                            } else MessageUtils.getInstance().sendMessageList(sender, "INVALID_INTEGER");
                        } else
                            MessageUtils.getInstance().sendMessageList(sender, "KEY_NOT_FOUND");
                    } else MessageUtils.getInstance().sendMessageList(sender, "PLAYER_NOT_FOUND");
                }
            } else MessageUtils.getInstance().sendMessageList(sender, "CRATE_ADMIN_USAGE");
        }
        return false;
    }

}
