package org.c0d3r.crates.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.c0d3r.crates.Main;
import org.c0d3r.crates.crate.Crate;
import org.c0d3r.crates.data.DataManager;
import org.c0d3r.crates.data.DataPlayer;
import org.c0d3r.crates.menu.CrateMenu;
import org.c0d3r.crates.utils.MessageUtils;
import org.c0d3r.crates.utils.Utils;

public class CratesCommand implements CommandExecutor {

    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (cmd.getName().equalsIgnoreCase("crates")) {
            if (sender instanceof Player) {
                Player player = (Player) sender;
                if (Main.getInstance().getConfig().getString("mode").equalsIgnoreCase("physical")) {
                    MessageUtils.getInstance().sendMessageList(player, "VIRTUAL_DISABLED");
                    return false;
                }
                DataPlayer dataPlayer = DataManager.getInstance().getByPlayer(player);
                if (dataPlayer == null) {
                    System.out.print("[C0d3rCrates] The player tried to accessed a command but was denied because the player didn't exist in the database.");
                    return false;
                }
                if (args.length == 0) {
                    for (String msg : Main.getInstance().getConfig().getStringList("messages.CRATE_BALANCE")) {
                        msg = Utils.toColor(msg);
                        if (msg.equals("%printcrate%")) {
                            for (Crate crate : Main.getInstance().getCrates().getCrateList()) {
                                String crateName = Main.getInstance().getCrates().crateToString(crate);
                                int amount = dataPlayer.getKey(crateName);
                                String format = Main.getInstance().getConfig().getString("messages.CRATE_BALANCE_PRINT");
                                format = format.replace("%crate%", crateName).replace("%amount%", Integer.toString(amount));
                                format = Utils.toColor(format);
                                player.sendMessage(format);
                            }
                        } else {
                            player.sendMessage(msg);
                        }
                    }
                } else {
                    if (args.length == 1) {
                        if (args[0].equalsIgnoreCase("help")) {
                            MessageUtils.getInstance().sendMessageList(player, "CRATES_HELP");
                        } else if (args[0].equalsIgnoreCase("open")) {
                            if (Utils.canUse(player)) {
                                new CrateMenu(player, dataPlayer);
                            }
                        } else MessageUtils.getInstance().sendMessageList(player, "CRATES_USAGE");
                    }
                }
            } else sender.sendMessage("§4Only players are allowed to execute this command.");
        }
        return false;
    }


}
