package org.c0d3r.crates.update;

import org.c0d3r.crates.Main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;

public class UpdateCheck {

    private String url;
    private String currentVersion;
    private boolean updated;
    private String newVersion;

    public UpdateCheck(String url, String currentVersion) {
        this.url = url;
        this.currentVersion = currentVersion;
        updated = getVersionFromURL().equalsIgnoreCase(currentVersion);
    }

    public void checkUpdate() {
        if (isUpdated()) {
            System.out.print("[C0d3rCrates] You are using the updated version of C0d3rCrates!");
        } else {
            System.out.print("[C0d3rCrates] You are using an outdated version of C0d3rCrates!");
            System.out.print("[C0d3rCrates] Current Version: " + Main.getInstance().getDescription().getVersion());
            System.out.print("[C0d3rCrates] New Version: " + getVersionFromURL());
        }
    }

    public boolean isUpdated() {
        return updated;
    }

    public String getVersionFromURL() {
        if (newVersion != null) return newVersion;
        try {
            URLConnection localURLConnection = new URL(url).openConnection();
            localURLConnection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11");
            localURLConnection.connect();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(localURLConnection.getInputStream(), Charset.forName("UTF-8")));
            newVersion = bufferedReader.readLine();
            return newVersion;
        } catch (IOException e) {
            System.out.print("You do not have a valid internet connection to check for an update.");
            e.printStackTrace();
        }
        return "Update check failed.";
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    private String getCurrentVersion() {
        return currentVersion;
    }

    public void setCurrentVersion(String currentVersion) {
        this.currentVersion = currentVersion;
    }
}
