package org.c0d3r.crates.animation;

import org.bukkit.inventory.ItemStack;
import org.c0d3r.crates.crate.CrateReward;
import org.c0d3r.crates.utils.RandomPick;

public interface Animation {

    void start();

    void stop();

    void setBorder(boolean random);

    void changeContents(int end, int start, ItemStack item);

    CrateReward pickReward();

    RandomPick<CrateReward> getRewards();

    void setRewards(RandomPick<CrateReward> rewards);

}
