package org.c0d3r.crates.animation.settings;

import java.util.HashMap;
import java.util.Random;

public class AnimationSettings {

    private transient Random random;
    private transient int currentSpeed;
    private transient int sb;
    private transient int currentLoops;
    private transient int maxLoops;
    private transient HashMap<Integer, Integer> speedList;

    public AnimationSettings() {
        this.random = new Random();
        this.currentSpeed = 1;
        this.sb = 0;
        this.currentLoops = 0;
        this.maxLoops = 100;
        (this.speedList = new HashMap<>()).put(0, 1);
        double SPEED = 1.3;
        double BASE = 12.3;
        this.maxLoops *= (int) SPEED;
        for (int index = 1; index <= 40; index++) {
            int base = (int) (index + random.nextInt(index) / 1.5);
            int speed = (int) (base * SPEED);
            if (index == 20) {
                base *= 0.85;
            } else if (index == 30) {
                base *= 0.65;
            } else if (index == 40) {
                base *= 0.45;
            }
            this.speedList.put((int) (base * BASE), speed);
        }
    }

    public boolean should() {
        ++this.currentLoops;
        ++this.sb;
        if (this.speedList.get(this.currentLoops) != null) {
            this.currentSpeed = this.speedList.get(this.currentLoops);
        }
        if (this.sb >= this.currentSpeed) {
            this.sb = 0;
            return true;
        }
        return false;
    }

    public void check() {
        if (this.currentLoops >= this.maxLoops) {
            this.currentSpeed += 2;
        }
    }

    public void destroy() {
        this.random = null;
        this.currentLoops = 0;
        this.sb = 0;
        this.currentSpeed = 0;
        this.maxLoops = 0;
        this.speedList.clear();
        this.speedList = null;
    }

}
