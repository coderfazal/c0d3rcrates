package org.c0d3r.crates.animation.pretask;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.Inventory;
import org.c0d3r.crates.Main;
import org.c0d3r.crates.animation.Animation;
import org.c0d3r.crates.animation.Animations;
import org.c0d3r.crates.animation.player.AnimationManager;
import org.c0d3r.crates.animation.player.AnimationPlayer;
import org.c0d3r.crates.animation.settings.AnimationSettings;
import org.c0d3r.crates.animation.types.ReverseCSGOSelectAnimation;
import org.c0d3r.crates.api.events.CratePreOpenEvent;
import org.c0d3r.crates.crate.Crate;
import org.c0d3r.crates.data.DataManager;
import org.c0d3r.crates.data.DataPlayer;
import org.c0d3r.crates.utils.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ReverseCSGOSelectSlotPick implements Listener {

    private static ReverseCSGOSelectSlotPick instance;
    private final List<String> bypassPlayers = new ArrayList<>();
    private final Map<String, List<Integer>> playerPickedMap = new HashMap<>();

    public static ReverseCSGOSelectSlotPick getInstance() {
        if (ReverseCSGOSelectSlotPick.instance == null) {
            synchronized (ReverseCSGOSelectSlotPick.class) {
                if (ReverseCSGOSelectSlotPick.instance == null) {
                    ReverseCSGOSelectSlotPick.instance = new ReverseCSGOSelectSlotPick();
                }
            }
        }
        return ReverseCSGOSelectSlotPick.instance;
    }

    @EventHandler
    public void onKick(PlayerKickEvent e) {
        this.bypassPlayers.remove(e.getPlayer().getName());
        this.playerPickedMap.remove(e.getPlayer().getName());
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent e) {
        this.bypassPlayers.remove(e.getPlayer().getName());
        this.playerPickedMap.remove(e.getPlayer().getName());
    }

    @EventHandler
    public void onClick(InventoryClickEvent e) {
        if (e.getCurrentItem() == null || e.getCurrentItem().getItemMeta() == null) {
            return;
        }
        if (e.getCurrentItem() == ItemUtils.getInstance().getUnusedItem()) return;
        Inventory inventory = e.getInventory();
        Player player = (Player) e.getWhoClicked();
        if (AnimationManager.hasAnimationPlayer(player)) {
            AnimationPlayer animationPlayer = AnimationManager.getAnimationPlayer(player);
            if (animationPlayer.getSlotTitle() == null) return;
            if (animationPlayer.getAnimations() == Animations.REVERSECSGOSELECT && inventory.getTitle().equalsIgnoreCase(animationPlayer.getSlotTitle())) {
                e.setCancelled(true);
                if (e.getCurrentItem().equals(ItemUtils.getInstance().getPickedItem())) {
                    if (this.playerPickedMap.containsKey(player.getName())) {
                        List<Integer> pickedSlots = this.playerPickedMap.get(player.getName());
                        if (pickedSlots.contains(e.getRawSlot())) {
                            pickedSlots.remove((Integer) e.getRawSlot());
                            this.playerPickedMap.remove(player.getName());
                            this.playerPickedMap.put(player.getName(), pickedSlots);
                            inventory.setItem(e.getRawSlot(), ItemUtils.getInstance().getUnPickedItem());
                        }
                    }
                    return;
                }
                if (e.getCurrentItem().equals(ItemUtils.getInstance().getUnusedItem())) return;
                Crate crate = animationPlayer.getCrate();
                Animation animation;
                Animations animations = Animations.REVERSECSGOSELECT;
                Inventory crateInventory;
                Runnable runnable = () -> bypassPlayers.remove(player.getName());
                if (this.playerPickedMap.containsKey(player.getName())) {
                    if (this.playerPickedMap.get(player.getName()).size() + 1 >= crate.getMaxRewardsReverseCSGOSelect()) {
                        List<Integer> pickedSlots = this.playerPickedMap.get(player.getName());
                        pickedSlots.add(e.getRawSlot());
                        AnimationSettings animationSettings = new AnimationSettings();
                        crateInventory = Bukkit.createInventory(null, animations.getInvSize(), Utils.toColor(crate.getTitle()));
                        animation = new ReverseCSGOSelectAnimation(player, crate, crateInventory, animationSettings, pickedSlots);
                        this.bypassPlayers.add(player.getName());
                        player.closeInventory();
                        CratePreOpenEvent.fireEvent(player, crate, animation);
                        this.playerPickedMap.remove(player.getName());
                        Bukkit.getScheduler().scheduleSyncDelayedTask(Main.getInstance(), runnable, 100L);
                    } else {
                        new SoundUtils(player).playSound("SLOT_PICK");
                        this.playerPickedMap.get(player.getName()).add(e.getRawSlot());
                        e.getInventory().setItem(e.getRawSlot(), ItemUtils.getInstance().getPickedItem());
                    }
                } else {
                    List<Integer> pickedSlots = new ArrayList<>();
                    pickedSlots.add(e.getRawSlot());
                    if (crate.getMaxRewardsReverseCSGOSelect() == 1) {
                        AnimationSettings animationSettings = new AnimationSettings();
                        crateInventory = Bukkit.createInventory(null, animations.getInvSize(), Utils.toColor(crate.getTitle()));
                        animation = new ReverseCSGOSelectAnimation(player, crate, crateInventory, animationSettings, pickedSlots);
                        this.bypassPlayers.add(player.getName());
                        player.closeInventory();
                        CratePreOpenEvent.fireEvent(player, crate, animation);
                        this.playerPickedMap.remove(player.getName());
                        Bukkit.getScheduler().scheduleSyncDelayedTask(Main.getInstance(), runnable, 100L);
                    } else {
                        this.playerPickedMap.put(player.getName(), pickedSlots);
                        e.getInventory().setItem(e.getRawSlot(), ItemUtils.getInstance().getPickedItem());
                        new SoundUtils(player).playSound("SLOT_PICK");
                    }
                }
            }
        }
    }

    @EventHandler
    public void slotPickerClose(InventoryCloseEvent e) {
        if (!(e.getPlayer() instanceof Player)) return;
        Player player = (Player) e.getPlayer();
        if (AnimationManager.hasAnimationPlayer(player) && AnimationManager.getAnimationPlayer(player).getAnimations() == Animations.REVERSECSGOSELECT && e.getInventory().getTitle().equalsIgnoreCase(AnimationManager.getAnimationPlayer(player).getSlotTitle())) {
            if (!bypassPlayers.contains(e.getPlayer().getName())) {
                AnimationPlayer animationPlayer = AnimationManager.getAnimationPlayer(player);
                if (Main.getInstance().getConfig().getString("mode").equalsIgnoreCase("virtual")) {
                    DataPlayer dataPlayer = DataManager.getInstance().getByPlayer(player);
                    if (dataPlayer == null) return;
                    dataPlayer.giveKeys(Main.getInstance().getCrates().crateToString(animationPlayer.getCrate()), 1);
                } else {
                    player.getInventory().addItem(animationPlayer.getCrate().getPhysicalKey()).values().forEach(drop -> player.getLocation().getWorld().dropItem(player.getLocation(), drop));
                    player.updateInventory();
                }
                MessageUtils.getInstance().sendMessageList(player, "KEY_REFUND", "%crate%", animationPlayer.getCrate().getCrateKey());
                animationPlayer.destroy();
            }
        }
    }

    public void openSlotPicker(Player player) {
        if (AnimationManager.hasAnimationPlayer(player)) {
            AnimationPlayer animationPlayer = AnimationManager.getAnimationPlayer(player);
            Inventory inventory = Bukkit.createInventory(null, 45, animationPlayer.getSlotTitle());
            InventoryUtils.fillInventory(inventory);
            for (int i = 4; i <= 40; i += 9) {
                inventory.setItem(i, ItemUtils.getInstance().getUnPickedItem());
            }
            player.openInventory(inventory);
        }
    }
}
