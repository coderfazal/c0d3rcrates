package org.c0d3r.crates.animation.types;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;
import org.c0d3r.crates.Main;
import org.c0d3r.crates.animation.Animation;
import org.c0d3r.crates.animation.player.AnimationManager;
import org.c0d3r.crates.crate.Crate;
import org.c0d3r.crates.crate.CrateReward;
import org.c0d3r.crates.utils.MessageUtils;
import org.c0d3r.crates.utils.RandomPick;
import org.c0d3r.crates.utils.SoundUtils;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class ShrinkAnimation extends BukkitRunnable implements Animation {


    private final Crate crate;
    private int step;
    private long startTime;
    private WeakReference<Player> player;
    private Inventory inventory;
    private SoundUtils soundUtils;
    private Random random;
    private int topBorder;
    private int bottomBorder;
    private List<Integer> borderSlots;
    private RandomPick<CrateReward> rewards;

    public ShrinkAnimation(Player player, Crate crate, Inventory inventory) {
        this.step = 0;
        this.inventory = inventory;
        this.crate = crate;
        this.startTime = 0L;
        this.player = new WeakReference<>(player);
        this.soundUtils = new SoundUtils(player);
        this.random = new Random();
        this.topBorder = 0;
        this.bottomBorder = 44;
        this.borderSlots = new ArrayList<>();
        this.rewards = rewards;
    }

    public CrateReward pickReward() {
        return rewards.next();
    }

    public RandomPick<CrateReward> getRewards() {
        return rewards;
    }

    public void setRewards(RandomPick<CrateReward> rewards) {
        this.rewards = rewards;
    }


    public void run() {
        if (this.player == null || this.player.get() == null || Bukkit.getPlayer(this.player.get().getName()) == null) {
            this.cancel();
            return;
        }
        Player player = this.player.get();
        if (player.getOpenInventory() == null || !player.getOpenInventory().getTopInventory().getName().equalsIgnoreCase(inventory.getTitle())) {
            player.closeInventory();
            player.openInventory(inventory);
        }
        long timeElapsed = System.currentTimeMillis() - this.startTime;
        if (this.step == 0) {
            if (timeElapsed < 6000L) {
                soundUtils.playSound("MOVING_ITEM");
                this.changeContents(this.topBorder, this.bottomBorder, null);
                player.updateInventory();
                topBorder++;
                bottomBorder--;
                this.borderSlots.add(topBorder);
                this.borderSlots.add(bottomBorder);
                if (Main.getInstance().getConfig().getBoolean("animations.SHRINK.sync-panes")) {
                    this.setBorder(false);
                } else {
                    this.setBorder(true);
                }
            } else if (timeElapsed >= 6500L) {
                soundUtils.playSound("ANIMATION_COMPLETE");
                topBorder++;
                bottomBorder--;
                this.borderSlots.add(topBorder);
                this.borderSlots.add(bottomBorder);
                ++this.step;
                if (borderSlots.contains(22)) borderSlots.remove(22);
            }
        } else if (step == 1) {
            if (timeElapsed < 9000L) {
                this.setBorder(false);
            } else {
                soundUtils.playSound("ANIM_INV_CLOSE");
                ItemStack item = inventory.getItem(22);
                if (item == null) {
                    inventory.setItem(22, pickReward().getItem());
                }
                CrateReward reward = crate.rewardFromItemStack(item);
                if (reward == null) {
                    reward = pickReward();
                    inventory.setItem(22, reward.getItem());
                }
                for (String command : reward.getCommandList()) {
                    command = command.replace("%player%", player.getName());
                    Bukkit.dispatchCommand(Bukkit.getConsoleSender(), command);
                }
                MessageUtils.getInstance().sendMessageList(player, "REWARD_RECEIVE", "%prize%", reward.getItem().getItemMeta().getDisplayName());
                player.closeInventory();
                AnimationManager.getAnimationPlayer(player).destroy();
                this.cancel();
            }
        }
    }

    public void start() {
        this.startTime = System.currentTimeMillis();
        Player player = this.player.get();
        for (int i = 0; i < inventory.getSize(); i++) {
            inventory.setItem(i, pickReward().getItem());
        }
        player.openInventory(inventory);
        this.borderSlots.add(0);
        this.borderSlots.add(44);
        player.updateInventory();
        this.runTaskTimer(Main.getInstance(), 1L, 6L);
    }

    public void stop() {
        if (this.step != 0) {
            this.step = 0;
        }
        if (this.startTime != 0L) {
            this.startTime = 0L;
        }
        if (this.player != null) {
            this.player.clear();
            this.player = null;
        }
        if (this.inventory != null) {
            inventory.clear();
            inventory = null;
        }
        if (this.random != null) {
            this.random = null;
        }
        if (this.soundUtils != null) {
            this.soundUtils = null;
        }
        if (this.borderSlots != null) {
            this.borderSlots.clear();
            this.borderSlots = null;
        }
        if (this.bottomBorder != 0) {
            this.bottomBorder = 0;
        }
        if (this.topBorder != 0) {
            this.topBorder = 0;
        }
    }

    public void setBorder(boolean random) {
        ItemStack border = crate.getBorderItem();
        int borderData = crate.getBorderDataList().get(ThreadLocalRandom.current().nextInt(crate.getBorderDataList().size()));
        for (int i = 0; i < inventory.getSize(); i++) {
            if (this.borderSlots.contains(i) && i != 22) {
                if (!random) {
                    border.setDurability((short) borderData);
                    inventory.setItem(i, border);
                } else {
                    borderData = crate.getBorderDataList().get(ThreadLocalRandom.current().nextInt(crate.getBorderDataList().size()));
                    border.setDurability((short) borderData);
                    inventory.setItem(i, border);
                }
            }
        }
    }

    public void cancel() {
        this.stop();
        super.cancel();
    }

    public void changeContents(int end, int start, ItemStack item) {
        if (item == null) {
            for (int i = 0; i < inventory.getSize(); i++) {
                if (i != start && i != end && !this.borderSlots.contains(i)) {
                    inventory.setItem(i, pickReward().getItem());
                }
            }
        }
    }
}
