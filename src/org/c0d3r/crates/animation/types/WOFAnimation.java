package org.c0d3r.crates.animation.types;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;
import org.c0d3r.crates.Main;
import org.c0d3r.crates.animation.Animation;
import org.c0d3r.crates.animation.player.AnimationManager;
import org.c0d3r.crates.animation.settings.AnimationSettings;
import org.c0d3r.crates.crate.Crate;
import org.c0d3r.crates.crate.CrateReward;
import org.c0d3r.crates.utils.MessageUtils;
import org.c0d3r.crates.utils.RandomPick;
import org.c0d3r.crates.utils.SoundUtils;

import java.lang.ref.WeakReference;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

public class WOFAnimation extends BukkitRunnable implements Animation {


    private final Crate crate;
    private final AnimationSettings animationSettings;
    private int step;
    private long startTime;
    private WeakReference<Player> player;
    private Inventory inventory;
    private SoundUtils soundUtils;
    private Random random;
    private Map<Integer, CrateReward> slotCrateRewardMap;
    private List<Integer> rewardSlotList;
    private int currentSlot;
    private RandomPick<CrateReward> rewards;

    public WOFAnimation(Player player, Crate crate, Inventory inventory, AnimationSettings animationSettings) {
        this.step = 0;
        this.inventory = inventory;
        this.crate = crate;
        this.startTime = 0L;
        this.player = new WeakReference<>(player);
        this.animationSettings = animationSettings;
        this.soundUtils = new SoundUtils(player);
        this.random = new Random();
        this.slotCrateRewardMap = new HashMap<>();
        this.rewardSlotList = new ArrayList<>();
        this.rewardSlotList.addAll(Arrays.asList(10, 11, 12, 13, 14, 15, 16, 25, 34, 43, 42, 41, 40, 39, 38, 37, 28, 19));
        this.currentSlot = this.rewardSlotList.get(random.nextInt(this.rewardSlotList.size()));
        this.rewards = rewards;
    }

    public CrateReward pickReward() {
        return rewards.next();
    }

    public RandomPick<CrateReward> getRewards() {
        return rewards;
    }

    public void setRewards(RandomPick<CrateReward> rewards) {
        this.rewards = rewards;
    }


    public void run() {
        if (this.player == null || this.player.get() == null || Bukkit.getPlayer(this.player.get().getName()) == null) {
            this.cancel();
            return;
        }
        Player player = this.player.get();
        if (player.getOpenInventory() == null || !player.getOpenInventory().getTopInventory().getName().equalsIgnoreCase(inventory.getTitle())) {
            player.closeInventory();
            player.openInventory(inventory);
        }
        long timeElapsed = System.currentTimeMillis() - this.startTime;
        if (this.step == 0) {
            if (timeElapsed < 6000L) {
                if (Main.getInstance().getConfig().getBoolean("animations.WHEELOFFORTUNE.sync-panes")) {
                    this.setBorder(false);
                } else {
                    this.setBorder(true);
                }
                animationSettings.check();
                if (animationSettings.should()) {
                    soundUtils.playSound("MOVING_ITEM");
                    int slotChange = calculateNextSlot(currentSlot);
                    this.changeContents(slotChange, this.currentSlot, null);
                    this.currentSlot = slotChange;
                    player.updateInventory();
                }
            } else if (timeElapsed >= 6500L) {
                inventory.setItem(currentSlot, this.slotCrateRewardMap.get(currentSlot).getItem());
                for (Integer slot : this.rewardSlotList) {
                    if (slot != this.currentSlot) {
                        inventory.setItem(slot, new ItemStack(Material.AIR, 1));
                    }
                }
                soundUtils.playSound("ANIMATION_COMPLETE");
                ++this.step;
            }
        } else if (step == 1) {
            if (timeElapsed < 9000L) {
                this.setBorder(false);
            } else {
                soundUtils.playSound("ANIM_INV_CLOSE");
                ItemStack item = inventory.getItem(currentSlot);
                if (item == null) {
                    inventory.setItem(currentSlot, pickReward().getItem());
                }
                CrateReward reward = crate.rewardFromItemStack(item);
                if (reward == null) {
                    reward = pickReward();
                    inventory.setItem(currentSlot, reward.getItem());
                }
                for (String command : reward.getCommandList()) {
                    command = command.replace("%player%", player.getName());
                    Bukkit.dispatchCommand(Bukkit.getConsoleSender(), command);
                }
                MessageUtils.getInstance().sendMessageList(player, "REWARD_RECEIVE", "%prize%", reward.getItem().getItemMeta().getDisplayName());
                player.closeInventory();
                AnimationManager.getAnimationPlayer(player).destroy();
                this.cancel();
                animationSettings.destroy();
            }
        }
    }

    private int calculateNextSlot(int slot) {
        int index = this.rewardSlotList.indexOf(slot) + 1;
        if (index > this.rewardSlotList.size() - 1) {
            index = 0;
        }
        return this.rewardSlotList.get(index);
    }

    public void start() {
        this.startTime = System.currentTimeMillis();
        Player player = this.player.get();
        ItemStack prizeItem = crate.getPrizeItem();
        for (Integer slot : this.rewardSlotList) {
            CrateReward reward = pickReward();
            this.slotCrateRewardMap.put(slot, reward);
            inventory.setItem(slot, reward.getItem());
        }
        int startPoint = this.rewardSlotList.get(random.nextInt(this.rewardSlotList.size()));
        this.currentSlot = startPoint;
        ItemStack itemStack = inventory.getItem(startPoint);
        itemStack.setType(Material.STAINED_GLASS_PANE);
        itemStack.setDurability((short) 5);
        inventory.setItem(startPoint, itemStack);
        player.openInventory(inventory);
        player.updateInventory();
        this.runTaskTimer(Main.getInstance(), 1L, 1L);
    }

    public void stop() {
        if (this.step != 0) {
            this.step = 0;
        }
        if (this.startTime != 0L) {
            this.startTime = 0L;
        }
        if (this.player != null) {
            this.player.clear();
            this.player = null;
        }
        if (this.rewardSlotList != null) {
            rewardSlotList.clear();
            rewardSlotList = null;
        }
        if (this.currentSlot != 0) {
            this.currentSlot = 0;
        }
        if (this.inventory != null) {
            inventory.clear();
            inventory = null;
        }
        if (this.random != null) {
            this.random = null;
        }
        if (this.soundUtils != null) {
            this.soundUtils = null;
        }
        if (this.slotCrateRewardMap != null) {
            slotCrateRewardMap.clear();
            slotCrateRewardMap = null;
        }
    }

    public void setBorder(boolean random) {
        ItemStack border = crate.getBorderItem();
        int borderData = crate.getBorderDataList().get(ThreadLocalRandom.current().nextInt(crate.getBorderDataList().size()));
        for (int i = 0; i < inventory.getSize(); i++) {
            if (!this.rewardSlotList.contains(i)) {
                if (!random) {
                    border.setDurability((short) borderData);
                    inventory.setItem(i, border);
                } else {
                    borderData = crate.getBorderDataList().get(ThreadLocalRandom.current().nextInt(crate.getBorderDataList().size()));
                    border.setDurability((short) borderData);
                    inventory.setItem(i, border);
                }
            }
        }
    }

    public void cancel() {
        this.stop();
        super.cancel();
    }

    public void changeContents(int start, int end, ItemStack item) {
        if (item == null) {
            inventory.setItem(end, this.slotCrateRewardMap.get(end).getItem());
            ItemStack itemStack = inventory.getItem(start);
            itemStack.setType(Material.STAINED_GLASS_PANE);
            itemStack.setDurability((short) 5);
            inventory.setItem(start, itemStack);
        }
    }
}
