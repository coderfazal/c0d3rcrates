package org.c0d3r.crates.animation.types;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;
import org.c0d3r.crates.Main;
import org.c0d3r.crates.animation.Animation;
import org.c0d3r.crates.animation.player.AnimationManager;
import org.c0d3r.crates.animation.settings.AnimationSettings;
import org.c0d3r.crates.crate.Crate;
import org.c0d3r.crates.crate.CrateReward;
import org.c0d3r.crates.utils.MessageUtils;
import org.c0d3r.crates.utils.RandomPick;
import org.c0d3r.crates.utils.SoundUtils;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class SlotAnimation extends BukkitRunnable implements Animation {


    private final Crate crate;
    private List<Integer> rewardSlots;
    private int step;
    private long startTime;
    private WeakReference<Player> player;
    private Inventory inventory;
    private AnimationSettings animationSettings;
    private SoundUtils soundUtils;
    private RandomPick<CrateReward> rewards;

    public SlotAnimation(Player player, Crate crate, Inventory inventory, AnimationSettings animationSettings) {
        this.step = 0;
        this.inventory = inventory;
        this.crate = crate;
        this.startTime = 0L;
        this.player = new WeakReference<>(player);
        this.animationSettings = animationSettings;
        this.soundUtils = new SoundUtils(player);
        this.rewardSlots = new ArrayList<>();
        this.rewardSlots.addAll(Arrays.asList(2, 4, 6, 11, 13, 15, 20, 22, 24, 29, 31, 33, 38, 40, 42));
        this.rewards = rewards;
    }

    public CrateReward pickReward() {
        return rewards.next();
    }

    public RandomPick<CrateReward> getRewards() {
        return rewards;
    }

    public void setRewards(RandomPick<CrateReward> rewards) {
        this.rewards = rewards;
    }


    public void run() {
        if (this.player == null || this.player.get() == null || Bukkit.getPlayer(this.player.get().getName()) == null) {
            this.cancel();
            return;
        }
        Player player = this.player.get();
        if (player.getOpenInventory() == null || !player.getOpenInventory().getTopInventory().getName().equalsIgnoreCase(inventory.getTitle())) {
            player.closeInventory();
            player.openInventory(inventory);
        }
        long timeElapsed = System.currentTimeMillis() - this.startTime;
        if (this.step == 0) {
            if (timeElapsed < 6000L) {
                if (Main.getInstance().getConfig().getBoolean("animations.SLOT.sync-panes")) {
                    this.setBorder(false);
                } else {
                    this.setBorder(true);
                }
                animationSettings.check();
                if (animationSettings.should()) {
                    soundUtils.playSound("MOVING_ITEM");
                    this.changeContents(2, 38, pickReward().getItem());
                    this.changeContents(4, 40, pickReward().getItem());
                    this.changeContents(6, 42, pickReward().getItem());
                    player.updateInventory();
                }
            } else if (timeElapsed >= 6500L) {
                soundUtils.playSound("ANIMATION_COMPLETE");
                for (int clearSlot : rewardSlots) {
                    if (clearSlot != 20 && clearSlot != 22 && clearSlot != 24) {
                        inventory.setItem(clearSlot, new ItemStack(Material.AIR, 1));
                    }
                }
                ++this.step;
            }
        } else if (step == 1) {
            if (timeElapsed < 9000L) {
                this.setBorder(false);
            } else {
                soundUtils.playSound("ANIM_INV_CLOSE");
                List<Integer> rewardsSlots = Arrays.asList(20, 22, 24);
                for (int slot : rewardsSlots) {
                    ItemStack item = inventory.getItem(slot);
                    if (item == null) {
                        inventory.setItem(slot, pickReward().getItem());
                    }
                    CrateReward reward = crate.rewardFromItemStack(item);
                    if (reward == null) {
                        reward = pickReward();
                        inventory.setItem(slot, reward.getItem());
                    }
                    for (String command : reward.getCommandList()) {
                        command = command.replace("%player%", player.getName());
                        Bukkit.dispatchCommand(Bukkit.getConsoleSender(), command);
                    }
                    MessageUtils.getInstance().sendMessageList(player, "REWARD_RECEIVE", "%prize%", reward.getItem().getItemMeta().getDisplayName());
                }
                player.closeInventory();
                AnimationManager.getAnimationPlayer(player).destroy();
                animationSettings.destroy();
                this.cancel();
            }
        }
    }

    public void start() {
        this.startTime = System.currentTimeMillis();
        Player player = this.player.get();
        ItemStack prizeItem = crate.getPrizeItem();
        inventory.setItem(19, prizeItem);
        inventory.setItem(21, prizeItem);
        inventory.setItem(23, prizeItem);
        inventory.setItem(25, prizeItem);
        for (int rewardSlot : rewardSlots) {
            inventory.setItem(rewardSlot, pickReward().getItem());
        }
        player.openInventory(inventory);
        player.updateInventory();
        this.runTaskTimer(Main.getInstance(), 1L, 1L);
    }

    public void stop() {
        if (this.step != 0) {
            this.step = 0;
        }
        if (this.startTime != 0L) {
            this.startTime = 0L;
        }
        if (this.player != null) {
            this.player.clear();
            this.player = null;
        }
        if (this.inventory != null) {
            inventory.clear();
            inventory = null;
        }
        if (this.soundUtils != null) {
            this.soundUtils = null;
        }
        if (this.rewardSlots != null) {
            this.rewardSlots.clear();
            this.rewardSlots = null;
        }
        if (this.animationSettings != null) {
            this.animationSettings = null;
        }
    }

    public void setBorder(boolean random) {
        ItemStack border = crate.getBorderItem();
        int borderData = crate.getBorderDataList().get(ThreadLocalRandom.current().nextInt(crate.getBorderDataList().size()));
        for (int i = 0; i < inventory.getSize(); i++) {
            if (!this.rewardSlots.contains(i)) {
                if (i != 23 && i != 19 && i != 21 && i != 25) {
                    if (!random) {
                        border.setDurability((short) borderData);
                        inventory.setItem(i, border);
                    } else {
                        borderData = crate.getBorderDataList().get(ThreadLocalRandom.current().nextInt(crate.getBorderDataList().size()));
                        border.setDurability((short) borderData);
                        inventory.setItem(i, border);
                    }
                }
            }
        }
    }

    public void cancel() {
        this.stop();
        super.cancel();
    }

    public void changeContents(int end, int start, ItemStack item) {
        for (int index = start; index > end; index -= 9) {
            inventory.setItem(index, inventory.getItem(index - 9));
        }
        inventory.setItem(end, item);
    }
}
