package org.c0d3r.crates.animation.types;

import com.gmail.filoghost.holographicdisplays.api.Hologram;
import com.gmail.filoghost.holographicdisplays.api.line.ItemLine;
import com.gmail.filoghost.holographicdisplays.api.line.TextLine;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;
import org.c0d3r.crates.Main;
import org.c0d3r.crates.animation.Animation;
import org.c0d3r.crates.animation.player.AnimationManager;
import org.c0d3r.crates.animation.settings.AnimationSettings;
import org.c0d3r.crates.crate.Crate;
import org.c0d3r.crates.crate.CrateReward;
import org.c0d3r.crates.utils.MessageUtils;
import org.c0d3r.crates.utils.RandomPick;
import org.c0d3r.crates.utils.SoundUtils;
import org.c0d3r.crates.utils.Utils;

import java.lang.ref.WeakReference;
import java.util.Random;

public class PhsyicalAnimation extends BukkitRunnable implements Animation {

    private final Crate crate;
    private int step;
    private long startTime;
    private WeakReference<Player> player;
    private AnimationSettings animationSettings;
    private SoundUtils soundUtils;
    private Random random;
    private Hologram hologram;
    private RandomPick<CrateReward> rewards;

    public PhsyicalAnimation(Player player, Crate crate, Hologram hologram, AnimationSettings animationSettings) {
        this.step = 0;
        this.crate = crate;
        this.startTime = 0L;
        this.hologram = hologram;
        this.player = new WeakReference<>(player);
        this.animationSettings = animationSettings;
        this.soundUtils = new SoundUtils(player);
        this.random = new Random();
        this.rewards = rewards;
    }

    public CrateReward pickReward() {
        return rewards.next();
    }

    public RandomPick<CrateReward> getRewards() {
        return rewards;
    }

    public void setRewards(RandomPick<CrateReward> rewards) {
        this.rewards = rewards;
    }


    public void run() {
        if (this.player == null || this.player.get() == null || Bukkit.getPlayer(this.player.get().getName()) == null) {
            this.cancel();
            return;
        }
        Player player = this.player.get();
        long timeElapsed = System.currentTimeMillis() - this.startTime;
        if (this.step == 0) {
            if (timeElapsed < 6000L) {
                animationSettings.check();
                if (animationSettings.should()) {
                    soundUtils.playSound("MOVING_ITEM");
                    CrateReward reward = pickReward();
                    ((TextLine) hologram.getLine(3)).setText(reward.getItem().getItemMeta().getDisplayName());
                    ((ItemLine) hologram.getLine(4)).setItemStack(reward.getItem());
                    player.updateInventory();
                }
            } else if (timeElapsed >= 6500L) {
                soundUtils.playSound("ANIMATION_COMPLETE");
                ++this.step;
            }
        } else if (step == 1) {
            if (timeElapsed < 9000L) {
                this.setBorder(false);
            } else {
                soundUtils.playSound("ANIM_INV_CLOSE");
                ItemStack item = ((ItemLine) hologram.getLine(4)).getItemStack();
                if (item == null) {
                    CrateReward reward = pickReward();
                    ((TextLine) hologram.getLine(3)).setText(reward.getItem().getItemMeta().getDisplayName());
                    ((ItemLine) hologram.getLine(4)).setItemStack(reward.getItem());
                }
                CrateReward reward = crate.rewardFromItemStack(item);
                if (reward == null) {
                    reward = pickReward();
                    ((TextLine) hologram.getLine(3)).setText(reward.getItem().getItemMeta().getDisplayName());
                    ((ItemLine) hologram.getLine(4)).setItemStack(pickReward().getItem());
                }
                for (String command : reward.getCommandList()) {
                    command = command.replace("%player%", player.getName());
                    Bukkit.dispatchCommand(Bukkit.getConsoleSender(), command);
                }
                MessageUtils.getInstance().sendMessageList(player, "REWARD_RECEIVE", "%prize%", reward.getItem().getItemMeta().getDisplayName());
                player.closeInventory();
                AnimationManager.getAnimationPlayer(player).destroy();
                animationSettings.destroy();
                this.cancel();
            }
        }
    }

    public void start() {
        this.startTime = System.currentTimeMillis();
        Player player = this.player.get();
        hologram.appendTextLine(Utils.toColor(Main.getInstance().getConfig().getString("animations.PHYSICAL.settings.hologramFormat").replace("%player%", player.getName()).replace("%crate%", crate.getCrateKey())));
        hologram.appendTextLine("");
        hologram.appendTextLine("");
        CrateReward crateReward = pickReward();
        hologram.appendTextLine(crateReward.getItem().getItemMeta().getDisplayName());
        hologram.appendItemLine(crateReward.getItem());
        this.runTaskTimer(Main.getInstance(), 1L, 1L);
    }

    public void stop() {
        if (this.step != 0) {
            this.step = 0;
        }
        if (this.startTime != 0L) {
            this.startTime = 0L;
        }
        if (this.player != null) {
            this.player.clear();
            this.player = null;
        }
        if (this.random != null) {
            this.random = null;
        }
        if (this.hologram != null) {
            this.hologram.delete();
            this.hologram = null;
        }
        if (this.soundUtils != null) {
            this.soundUtils = null;
        }
        if (this.animationSettings != null) {
            this.animationSettings = null;
        }
    }

    public void setBorder(boolean random) {
    }

    public void cancel() {
        this.stop();
        super.cancel();
    }

    public void changeContents(int end, int start, ItemStack item) {
    }

}
