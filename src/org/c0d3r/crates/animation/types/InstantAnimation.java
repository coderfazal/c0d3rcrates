package org.c0d3r.crates.animation.types;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.c0d3r.crates.animation.Animation;
import org.c0d3r.crates.animation.player.AnimationManager;
import org.c0d3r.crates.crate.Crate;
import org.c0d3r.crates.crate.CrateReward;
import org.c0d3r.crates.utils.MessageUtils;
import org.c0d3r.crates.utils.RandomPick;

public class InstantAnimation implements Animation {


    private final Crate crate;
    private Player player;
    private int multiplier;
    private RandomPick<CrateReward> rewards;

    public InstantAnimation(Player player, Crate crate, int multiplier) {
        this.crate = crate;
        this.player = player;
        this.multiplier = multiplier;
        this.rewards = rewards;
    }

    public InstantAnimation(Player player, Crate crate) {
        this(player, crate, 1);
    }

    public CrateReward pickReward() {
        return rewards.next();
    }

    public RandomPick<CrateReward> getRewards() {
        return rewards;
    }

    public void setRewards(RandomPick<CrateReward> rewards) {
        this.rewards = rewards;
    }


    @Override
    public void start() {
        for (int i = 0; i < multiplier; i++) {
            for (int j = 0; j < crate.getMaxRewardsInstant(); j++) {
                CrateReward crateReward = pickReward();
                MessageUtils.getInstance().sendMessageList(player, "REWARD_RECEIVE", "%prize%", crateReward.getItem().getItemMeta().getDisplayName());
                for (String command : crateReward.getCommandList()) {
                    command = command.replace("%player%", player.getName());
                    Bukkit.dispatchCommand(Bukkit.getConsoleSender(), command);
                }
            }
        }
        AnimationManager.getAnimationPlayer(player).destroy();
        this.stop();
    }

    @Override
    public void stop() {
        if (this.player != null) player = null;
    }

    @Override
    public void setBorder(boolean random) {

    }

    @Override
    public void changeContents(int end, int start, ItemStack item) {

    }

    public void setBorder() {
    }

    public void changeContents() {
    }
}
