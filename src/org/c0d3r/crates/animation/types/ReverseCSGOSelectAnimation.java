package org.c0d3r.crates.animation.types;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;
import org.c0d3r.crates.Main;
import org.c0d3r.crates.animation.Animation;
import org.c0d3r.crates.animation.player.AnimationManager;
import org.c0d3r.crates.animation.settings.AnimationSettings;
import org.c0d3r.crates.crate.Crate;
import org.c0d3r.crates.crate.CrateReward;
import org.c0d3r.crates.utils.MessageUtils;
import org.c0d3r.crates.utils.RandomPick;
import org.c0d3r.crates.utils.SoundUtils;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class ReverseCSGOSelectAnimation extends BukkitRunnable implements Animation {


    private final Crate crate;
    private final List<Integer> selectedSlots;
    private final List<Integer> prizeSlots;
    private int step;
    private long startTime;
    private WeakReference<Player> player;
    private Inventory inventory;
    private AnimationSettings animationSettings;
    private SoundUtils soundUtils;
    private Random random;
    private long timeElapsed;
    private RandomPick<CrateReward> rewards;

    public ReverseCSGOSelectAnimation(Player player, Crate crate, Inventory inventory, AnimationSettings animationSettings, List<Integer> slots) {
        this.step = 0;
        this.inventory = inventory;
        this.crate = crate;
        this.startTime = 0L;
        this.player = new WeakReference<>(player);
        this.animationSettings = animationSettings;
        this.soundUtils = new SoundUtils(player);
        this.random = new Random();
        this.selectedSlots = new ArrayList<>();
        this.prizeSlots = new ArrayList<>();
        this.selectedSlots.addAll(slots);
        timeElapsed = 0L;
        this.rewards = rewards;
    }

    public CrateReward pickReward() {
        return rewards.next();
    }

    public RandomPick<CrateReward> getRewards() {
        return rewards;
    }

    public void setRewards(RandomPick<CrateReward> rewards) {
        this.rewards = rewards;
    }


    public void run() {
        if (this.player == null || this.player.get() == null || Bukkit.getPlayer(this.player.get().getName()) == null) {
            this.cancel();
            return;
        }
        Player player = this.player.get();
        if (player.getOpenInventory() == null || !player.getOpenInventory().getTopInventory().getName().equalsIgnoreCase(inventory.getTitle())) {
            player.closeInventory();
            player.openInventory(inventory);
        }
        timeElapsed = System.currentTimeMillis() - this.startTime;
        if (this.step == 0) {
            if (timeElapsed < 6000L) {
                if (Main.getInstance().getConfig().getBoolean("animations.REVERSE-CSGO-SELECT.sync-panes")) {
                    this.setBorder(false);
                } else {
                    this.setBorder(true);
                }
                animationSettings.check();
                if (animationSettings.should()) {
                    soundUtils.playSound("MOVING_ITEM");
                    this.changeContents(4, 40, pickReward().getItem());
                    player.updateInventory();
                }
            } else if (timeElapsed >= 6500L) {
                soundUtils.playSound("ANIMATION_COMPLETE");
                prizeSlots.forEach(slot -> inventory.setItem(slot, crate.getPrizeItem()));
                for (int i = 4; i <= 40; i += 9) {
                    if (!this.selectedSlots.contains(i)) {
                        inventory.setItem(i, new ItemStack(Material.AIR, 1));
                    }
                }
                ++this.step;
            }
        } else if (step == 1) {
            if (timeElapsed < 9000L) {
                this.setBorder(false);
            } else {
                soundUtils.playSound("ANIM_INV_CLOSE");
                selectedSlots.forEach(slot -> {
                    ItemStack item = inventory.getItem(slot);
                    if (item == null) {
                        inventory.setItem(slot, pickReward().getItem());
                    }
                    CrateReward reward = crate.rewardFromItemStack(item);
                    if (reward == null) {
                        reward = pickReward();
                        inventory.setItem(slot, reward.getItem());
                    }
                    for (String command : reward.getCommandList()) {
                        command = command.replace("%player%", player.getName());
                        Bukkit.dispatchCommand(Bukkit.getConsoleSender(), command);
                    }
                    MessageUtils.getInstance().sendMessageList(player, "REWARD_RECEIVE", "%prize%", reward.getItem().getItemMeta().getDisplayName());
                });
                player.closeInventory();
                AnimationManager.getAnimationPlayer(player).destroy();
                animationSettings.destroy();
                this.cancel();
            }
        }
    }

    public void start() {
        this.startTime = System.currentTimeMillis();
        Player player = this.player.get();
        selectedSlots.forEach(slot -> this.prizeSlots.addAll(Arrays.asList(slot - 1, slot + 1)));
        inventory.setItem(4, pickReward().getItem());
        inventory.setItem(13, pickReward().getItem());
        inventory.setItem(22, pickReward().getItem());
        inventory.setItem(31, pickReward().getItem());
        inventory.setItem(40, pickReward().getItem());
        player.openInventory(inventory);
        player.updateInventory();
        this.runTaskTimer(Main.getInstance(), 1L, 1L);
    }

    public void stop() {
        if (this.step != 0) {
            this.step = 0;
        }
        if (this.startTime != 0L) {
            this.startTime = 0L;
        }
        if (this.player != null) {
            this.player.clear();
            this.player = null;
        }
        if (this.inventory != null) {
            inventory.clear();
            inventory = null;
        }
        if (this.random != null) {
            this.random = null;
        }
        if (this.soundUtils != null) {
            this.soundUtils = null;
        }
        if (this.animationSettings != null) {
            this.animationSettings = null;
        }
    }

    public void setBorder(boolean random) {
        ItemStack border = crate.getBorderItem();
        int borderData = crate.getBorderDataList().get(ThreadLocalRandom.current().nextInt(crate.getBorderDataList().size()));
        if (timeElapsed > 6500L) {
            for (int i = 0; i < inventory.getSize(); i++) {
                if (i != 4 && i != 13 && i != 22 && i != 31 && i != 40 && !prizeSlots.contains(i)) {
                    if (!random) {
                        border.setDurability((short) borderData);
                        inventory.setItem(i, border);
                    } else {
                        borderData = crate.getBorderDataList().get(ThreadLocalRandom.current().nextInt(crate.getBorderDataList().size()));
                        border.setDurability((short) borderData);
                        inventory.setItem(i, border);
                    }
                }
            }
            return;
        }
        for (int i = 0; i < inventory.getSize(); i++) {
            if (i != 4 && i != 13 && i != 22 && i != 31 && i != 40) {
                if (!random) {
                    border.setDurability((short) borderData);
                    inventory.setItem(i, border);
                } else {
                    borderData = crate.getBorderDataList().get(ThreadLocalRandom.current().nextInt(crate.getBorderDataList().size()));
                    border.setDurability((short) borderData);
                    inventory.setItem(i, border);
                }
            }
        }
    }

    public void cancel() {
        this.stop();
        super.cancel();
    }

    public void changeContents(int end, int start, ItemStack item) {
        for (int index = start; index > end; index -= 9) {
            inventory.setItem(index, inventory.getItem(index - 9));
        }
        inventory.setItem(end, item);
    }
}
