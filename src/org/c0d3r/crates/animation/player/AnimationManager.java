package org.c0d3r.crates.animation.player;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.c0d3r.crates.crate.Crate;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class AnimationManager implements Listener {

    private static Map<UUID, AnimationPlayer> animationPlayers;

    public static Map<UUID, AnimationPlayer> getAnimationPlayers() {
        if (animationPlayers == null) {
            animationPlayers = new HashMap<>();
        }
        return animationPlayers;
    }

    public static int getOpeningAmount() {
        return getAnimationPlayers().size();
    }

    public static void clearAnimationsPlayers() {
        for (Player player : Bukkit.getOnlinePlayers()) {
            destroyAnimationPlayer(player);
        }
        getAnimationPlayers().clear();
    }

    public static AnimationPlayer getAnimationPlayer(Player player) {
        for (AnimationPlayer animationPlayer : getAnimationPlayers().values()) {
            if (animationPlayer.getUuid().equals(player.getUniqueId())) {
                return animationPlayer;
            }
        }
        return null;
    }

    public static AnimationPlayer createAnimationPlayer(Player player, Crate crate) {
        AnimationPlayer animationPlayer = new AnimationPlayer(player, crate);
        animationPlayer.add();
        return animationPlayer;
    }

    public static boolean hasAnimationPlayer(Player player) {
        return getAnimationPlayer(player) != null;
    }

    public static void destroyAnimationPlayer(Player player) {
        if (!hasAnimationPlayer(player)) return;
        AnimationPlayer animationPlayer = getAnimationPlayer(player);
        animationPlayer.destroy();
        player.closeInventory();
    }


    @EventHandler
    public void onKick(PlayerKickEvent e) {
        if (hasAnimationPlayer(e.getPlayer())) {
            destroyAnimationPlayer(e.getPlayer());
        }
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent e) {
        if (hasAnimationPlayer(e.getPlayer())) {
            destroyAnimationPlayer(e.getPlayer());
        }
    }

}
