package org.c0d3r.crates.animation.player;

import org.bukkit.entity.Player;
import org.c0d3r.crates.animation.Animations;
import org.c0d3r.crates.crate.Crate;

import java.util.UUID;

public class AnimationPlayer {

    private UUID uuid;
    private Player player;
    private Crate crate;
    private Animations animations;
    private String slotTitle;

    public AnimationPlayer(Player player, Crate crate) {
        this.player = player;
        this.uuid = player.getUniqueId();
        this.crate = crate;
        this.animations = Animations.CSGO;
        this.slotTitle = null;
    }

    public void destroy() {
        AnimationManager.getAnimationPlayers().remove(getUuid());
        if (this.uuid != null) this.uuid = null;
        if (this.player != null) this.player = null;
        if (this.crate != null) this.crate = null;
        if (this.animations != null) this.animations = null;
        if (this.slotTitle != null) this.slotTitle = null;
    }

    public String getSlotTitle() {
        return slotTitle;
    }

    public void setSlotTitle(String slotTitle) {
        this.slotTitle = slotTitle;
    }

    public Animations getAnimations() {
        return animations;
    }

    public void setAnimations(Animations animations) {
        this.animations = animations;
    }

    public void add() {
        AnimationManager.getAnimationPlayers().put(getUuid(), this);
    }

    public Crate getCrate() {
        return crate;
    }

    public void setCrate(Crate crate) {
        this.crate = crate;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }
}
