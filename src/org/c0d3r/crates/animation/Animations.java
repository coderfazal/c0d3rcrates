package org.c0d3r.crates.animation;

import org.bukkit.inventory.ItemStack;
import org.c0d3r.crates.Main;
import org.c0d3r.crates.utils.ItemUtils;

public enum Animations {

    CSGO("CSGO", 0, 27, ItemUtils.getInstance().getAnimationItem("CSGO"), Main.getInstance().getConfig().getInt("animations.CSGO.slot")),
    REVERSECSGO("REVERSECSGO", 1, 45, ItemUtils.getInstance().getAnimationItem("REVERSECSGO"), Main.getInstance().getConfig().getInt("animations.REVERSECSGO.slot")),
    ROULETTE("ROULETTE", 2, 27, ItemUtils.getInstance().getAnimationItem("ROULETTE"), Main.getInstance().getConfig().getInt("animations.ROULETTE.slot")),
    WHEELOFFORTUNE("WHEELOFFORTUNE", 3, 54, ItemUtils.getInstance().getAnimationItem("WHEELOFFORTUNE"), Main.getInstance().getConfig().getInt("animations.WHEELOFFORTUNE.slot")),
    SHRINK("SHRINK", 4, 45, ItemUtils.getInstance().getAnimationItem("SHRINK"), Main.getInstance().getConfig().getInt("animations.SHRINK.slot")),
    INSTANT("INSTANT", 5, 0, ItemUtils.getInstance().getAnimationItem("INSTANT"), Main.getInstance().getConfig().getInt("animations.INSTANT.slot")),
    DOUBLECSGO("DOUBLECSGO", 6, 36, ItemUtils.getInstance().getAnimationItem("DOUBLECSGO"), Main.getInstance().getConfig().getInt("animations.DOUBLECSGO.slot")),
    THREEBYTHREE("3BY3", 7, 27, ItemUtils.getInstance().getAnimationItem("3BY3"), Main.getInstance().getConfig().getInt("animations.3BY3.slot")),
    SLOT("SLOT", 8, 45, ItemUtils.getInstance().getAnimationItem("SLOT"), Main.getInstance().getConfig().getInt("animations.SLOT.slot")),
    CSGOSELECT("CSGO-SELECT", 9, 27, ItemUtils.getInstance().getAnimationItem("CSGO-SELECT"), Main.getInstance().getConfig().getInt("animations.CSGO-SELECT.slot")),
    REVERSECSGOSELECT("REVERSE-CSGO-SELECT", 10, 45, ItemUtils.getInstance().getAnimationItem("REVERSE-CSGO-SELECT"), Main.getInstance().getConfig().getInt("animations.REVERSE-CSGO-SELECT.slot")),
    PHYSICAL("PHYSICAL", 11, 0, ItemUtils.getInstance().getAnimationItem("PHYSICAL"), Main.getInstance().getConfig().getInt("animations.PHYSICAL.slot"));

    private final int invSize;
    private String animationName;
    private ItemStack displayItem;
    private int slot;

    Animations(String animationName, int index, int invSize, ItemStack item, int slot) {
        this.invSize = invSize;
        this.animationName = animationName;
        this.displayItem = item;
        this.slot = slot;
    }

    public static Animations getByName(String name) {
        Animations[] values;
        for (int index = (values = values()).length, i = 0; i < index; ++i) {
            Animations animation = values[i];
            if (animation.getAnimationName().equalsIgnoreCase(name)) {
                return animation;
            }
        }
        return null;
    }

    public String getAnimationName() {
        return animationName;
    }

    public void setAnimationName(String animationName) {
        this.animationName = animationName;
    }

    public ItemStack getDisplayItem() {
        return displayItem;
    }

    public void setDisplayItem(ItemStack displayItem) {
        this.displayItem = displayItem;
    }

    public int getSlot() {
        return slot;
    }

    public void setSlot(int slot) {
        this.slot = slot;
    }

    public int getInvSize() {
        return invSize;
    }
}
