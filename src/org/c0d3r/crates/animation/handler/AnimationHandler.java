package org.c0d3r.crates.animation.handler;

import com.gmail.filoghost.holographicdisplays.api.HologramsAPI;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.Inventory;
import org.c0d3r.crates.Main;
import org.c0d3r.crates.animation.Animation;
import org.c0d3r.crates.animation.Animations;
import org.c0d3r.crates.animation.player.AnimationManager;
import org.c0d3r.crates.animation.player.AnimationPlayer;
import org.c0d3r.crates.animation.pretask.CSGOSelectSlotPick;
import org.c0d3r.crates.animation.pretask.ReverseCSGOSelectSlotPick;
import org.c0d3r.crates.animation.pretask.ThreeByThreeSlotPick;
import org.c0d3r.crates.animation.settings.AnimationSettings;
import org.c0d3r.crates.animation.types.*;
import org.c0d3r.crates.api.events.CratePreOpenEvent;
import org.c0d3r.crates.crate.Crate;
import org.c0d3r.crates.data.DataManager;
import org.c0d3r.crates.data.DataPlayer;
import org.c0d3r.crates.utils.*;

import java.util.ArrayList;
import java.util.List;

public class AnimationHandler implements Listener {

    private static AnimationHandler instance;
    private final List<String> bypassPlayers = new ArrayList<>();

    public static AnimationHandler getInstance() {
        if (AnimationHandler.instance == null) {
            synchronized (AnimationHandler.class) {
                if (AnimationHandler.instance == null) {
                    AnimationHandler.instance = new AnimationHandler();
                }
            }
        }
        return AnimationHandler.instance;
    }

    public void openAnimationPicker(Player player) {
        SoundUtils soundUtils = new SoundUtils(player);
        if (AnimationManager.hasAnimationPlayer(player)) {
            Inventory inventory = Bukkit.createInventory(null, Main.getInstance().getConfig().getInt("menus.animationPicker.size"), Utils.toColor(Main.getInstance().getConfig().getString("menus.animationPicker.title")));
            InventoryUtils.fillInventory(inventory);
            for (Animations animations : Animations.values()) {
                inventory.setItem(animations.getSlot(), animations.getDisplayItem());
            }
            player.openInventory(inventory);
            soundUtils.playSound("INV_ANIM_PICK_OPEN");
        }
    }

    @EventHandler
    public void animationClick(InventoryClickEvent e) {
        if (e.getCurrentItem() == null || e.getCurrentItem().getItemMeta() == null) {
            return;
        }
        if (e.getCurrentItem() == ItemUtils.getInstance().getUnusedItem()) return;
        Inventory inventory = e.getInventory();
        Player player = (Player) e.getWhoClicked();
        if (AnimationManager.hasAnimationPlayer(player)) {
            AnimationPlayer animationPlayer = AnimationManager.getAnimationPlayer(player);
            if (inventory.getTitle().equalsIgnoreCase(Utils.toColor(animationPlayer.getCrate().getTitle())))
                e.setCancelled(true);
        }
    }

    @EventHandler
    public void animationPickerClose(InventoryCloseEvent e) {
        if (!(e.getPlayer() instanceof Player)) return;
        if (e.getInventory().getTitle().equalsIgnoreCase(Utils.toColor(Main.getInstance().getConfig().getString("menus.animationPicker.title")))) {
            if (!bypassPlayers.contains(e.getPlayer().getName())) {
                Player player = (Player) e.getPlayer();
                if (AnimationManager.hasAnimationPlayer(player)) {
                    AnimationPlayer animationPlayer = AnimationManager.getAnimationPlayer(player);
                    if (Main.getInstance().getConfig().getString("mode").equalsIgnoreCase("virtual")) {
                        DataPlayer dataPlayer = DataManager.getInstance().getByPlayer(player);
                        if (dataPlayer == null) return;
                        dataPlayer.giveKeys(Main.getInstance().getCrates().crateToString(animationPlayer.getCrate()), 1);
                    } else {
                        player.getInventory().addItem(animationPlayer.getCrate().getPhysicalKey()).values().forEach(drop -> player.getLocation().getWorld().dropItem(player.getLocation(), drop));
                        player.updateInventory();
                    }
                    MessageUtils.getInstance().sendMessageList(player, "KEY_REFUND", "%crate%", animationPlayer.getCrate().getCrateKey());
                    animationPlayer.destroy();
                }
            }
        }
    }


    @EventHandler
    public void onKick(PlayerKickEvent e) {
        this.bypassPlayers.remove(e.getPlayer().getName());
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent e) {
        this.bypassPlayers.remove(e.getPlayer().getName());
    }

    @EventHandler
    public void animationPickerClick(InventoryClickEvent e) {
        if (e.getCurrentItem() == null || e.getCurrentItem().getItemMeta() == null) {
            return;
        }
        if (e.getCurrentItem() == ItemUtils.getInstance().getUnusedItem()) return;
        Inventory inventory = e.getInventory();
        Player player = (Player) e.getWhoClicked();
        if (AnimationManager.hasAnimationPlayer(player)) {
            AnimationPlayer animationPlayer = AnimationManager.getAnimationPlayer(player);
            if (inventory.getName().equalsIgnoreCase(Utils.toColor(Main.getInstance().getConfig().getString("menus.animationPicker.title")))) {
                e.setCancelled(true);
                Crate crate = animationPlayer.getCrate();
                Animations animations = null;
                for (Animations animationList : Animations.values()) {
                    if (e.getRawSlot() == animationList.getSlot()) {
                        animations = animationList;
                        break;
                    }
                }
                if (animations != null) {
                    animationPlayer.setAnimations(animations);
                    if (!crate.isAnimationEnabled(animations)) {
                        MessageUtils.getInstance().sendMessageList(player, "ANIMATION_NOT_ENABLED");
                        return;
                    }
                    Animation animation = null;
                    Inventory crateInventory;
                    Runnable runnable = () -> bypassPlayers.remove(player.getName());
                    if (animations == Animations.CSGO) {
                        crateInventory = Bukkit.createInventory(null, animations.getInvSize(), Utils.toColor(crate.getTitle()));
                        AnimationSettings animationSettings = new AnimationSettings();
                        animation = new CSGOAnimation(player, crate, crateInventory, animationSettings);
                    } else if (animations == Animations.INSTANT) {
                        animation = new InstantAnimation(player, crate);
                    } else if (animations == Animations.REVERSECSGO) {
                        crateInventory = Bukkit.createInventory(null, animations.getInvSize(), Utils.toColor(crate.getTitle()));
                        AnimationSettings animationSettings = new AnimationSettings();
                        animation = new ReverseCSGOAnimation(player, crate, crateInventory, animationSettings);
                    } else if (animations == Animations.ROULETTE) {
                        crateInventory = Bukkit.createInventory(null, animations.getInvSize(), Utils.toColor(crate.getTitle()));
                        AnimationSettings animationSettings = new AnimationSettings();
                        animation = new RouletteAnimation(player, crate, crateInventory, animationSettings);
                    } else if (animations == Animations.WHEELOFFORTUNE) {
                        crateInventory = Bukkit.createInventory(null, animations.getInvSize(), Utils.toColor(crate.getTitle()));
                        AnimationSettings animationSettings = new AnimationSettings();
                        animation = new WOFAnimation(player, crate, crateInventory, animationSettings);
                    } else if (animations == Animations.SHRINK) {
                        crateInventory = Bukkit.createInventory(null, animations.getInvSize(), Utils.toColor(crate.getTitle()));
                        animation = new ShrinkAnimation(player, crate, crateInventory);
                    } else if (animations == Animations.DOUBLECSGO) {
                        AnimationSettings animationSettings = new AnimationSettings();
                        crateInventory = Bukkit.createInventory(null, animations.getInvSize(), Utils.toColor(crate.getTitle()));
                        animation = new DoubleCSGOAnimation(player, crate, crateInventory, animationSettings);
                    } else if (animations == Animations.THREEBYTHREE) {
                        this.bypassPlayers.add(player.getName());
                        player.closeInventory();
                        animationPlayer.setSlotTitle(Utils.toColor(Main.getInstance().getConfig().getString("menus.slotPicker.title").replace("%slots%", crate.getMaxRewards3BY3() + "")));
                        ThreeByThreeSlotPick.getInstance().openSlotPicker(player);
                        Bukkit.getScheduler().scheduleSyncDelayedTask(Main.getInstance(), runnable, 100L);
                        return;
                    } else if (animations == Animations.SLOT) {
                        crateInventory = Bukkit.createInventory(null, animations.getInvSize(), Utils.toColor(crate.getTitle()));
                        AnimationSettings animationSettings = new AnimationSettings();
                        animation = new SlotAnimation(player, crate, crateInventory, animationSettings);
                    } else if (animations == Animations.CSGOSELECT) {
                        this.bypassPlayers.add(player.getName());
                        player.closeInventory();
                        animationPlayer.setSlotTitle(Utils.toColor(Main.getInstance().getConfig().getString("menus.slotPicker.title").replace("%slots%", crate.getMaxRewardsCSGOSelect() + "")));
                        CSGOSelectSlotPick.getInstance().openSlotPicker(player);
                        Bukkit.getScheduler().scheduleSyncDelayedTask(Main.getInstance(), runnable, 100L);
                        return;
                    } else if (animations == Animations.REVERSECSGOSELECT) {
                        this.bypassPlayers.add(player.getName());
                        player.closeInventory();
                        animationPlayer.setSlotTitle(Utils.toColor(Main.getInstance().getConfig().getString("menus.slotPicker.title").replace("%slots%", crate.getMaxRewardsReverseCSGOSelect() + "")));
                        ReverseCSGOSelectSlotPick.getInstance().openSlotPicker(player);
                        Bukkit.getScheduler().scheduleSyncDelayedTask(Main.getInstance(), runnable, 100L);
                        return;
                    } else if (animations == Animations.PHYSICAL) {
                        AnimationSettings animationSettings = new AnimationSettings();
                        animation = new PhsyicalAnimation(player, crate, HologramsAPI.createHologram(Main.getInstance(), player.getLocation().clone().add(0.5, 3.33, 0.5)), animationSettings);
                    }
                    this.bypassPlayers.add(player.getName());
                    player.closeInventory();
                    CratePreOpenEvent.fireEvent(player, crate, animation);
                    Bukkit.getScheduler().scheduleSyncDelayedTask(Main.getInstance(), runnable, 100L);
                }
            }
        }
    }

}
